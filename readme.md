# backend.spoor.link
This is the backend server for spoor.link. It can run alongside with the frontend website, or standalone with a custom frontend.

## Requirements
* NodeJs
* MySQL
* NDOV loket subscription
* NS API subscription