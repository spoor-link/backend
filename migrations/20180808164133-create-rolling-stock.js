'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('RollingStocks', {
            id: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true,
                allowNull: false,
            },
            description: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            unitId: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            length: {
                type: Sequelize.INTEGER,
                allowNull: true,
            },
            carriages: {
                type: Sequelize.INTEGER,
                allowNull: true,
            },
            isGroup: {
                type: Sequelize.BOOLEAN,
                allowNull: true,
            },
            familyId: {
                type: Sequelize.UUID,
                allowNull: true,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        }).then(() => {
            queryInterface.addIndex('RollingStocks', ['unitId']);
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('RollingStocks');
    }
};