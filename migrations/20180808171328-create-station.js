'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Stations', {
            id: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true,
                allowNull: false,
            },
            changeTime: {
                type: Sequelize.INTEGER,
                allowNull: true,
            },
            lat: {
                type: Sequelize.DOUBLE(10, 7),
                allowNull: false,
            },
            lon: {
                type: Sequelize.DOUBLE(10, 7),
                allowNull: false,
            },
            code: {
                type: Sequelize.STRING(45),
                allowNull: false,
            },
            shortName: {
                type: Sequelize.STRING(45),
                allowNull: true,
            },
            middleName: {
                type: Sequelize.STRING(45),
                allowNull: true,
            },
            longName: {
                type: Sequelize.STRING(45),
                allowNull: false,
            },
            passengers: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: true,
            },
            type: {
                type: Sequelize.INTEGER,
                allowNull: true,
            },
            countryId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        }).then(() => {
            queryInterface.addIndex('Stations', ['type']);
            queryInterface.addIndex('Stations', ['countryId']);
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Stations');
    }
};