'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('RouteEntryHasRollingStocks', {
            routeEntryId: {
                type: Sequelize.UUID,
                primaryKey: true,
                allowNull: false,
            },
            rollingStockId: {
                type: Sequelize.UUID,
                primaryKey: true,
                allowNull: false,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            }
        }).then(() => {
            queryInterface.addIndex('RouteEntryHasRollingStocks', ['rollingStockId']);
            queryInterface.addIndex('RouteEntryHasRollingStocks', ['routeEntryId']);
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('RouteEntryHasRollingStocks');
    }
};