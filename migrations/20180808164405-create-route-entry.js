'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('RouteEntries', {
            id: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true,
                allowNull: false,
            },
            stopType: {
                type: Sequelize.ENUM('f', 's', 'p'),
                allowNull: false,
            },
            announced: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            cancelledArrival: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            cancelledDeparture: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            plannedArrival: {
                type: Sequelize.DATE,
                allowNull: true,
            },
            currentArrival: {
                type: Sequelize.DATE,
                allowNull: true,
            },
            plannedDeparture: {
                type: Sequelize.DATE,
                allowNull: true,
            },
            currentDeparture: {
                type: Sequelize.DATE,
                allowNull: true,
            },
            plannedTrack: {
                type: Sequelize.STRING(45),
                allowNull: true,
            },
            currentTrack: {
                type: Sequelize.STRING(45),
                allowNull: true,
            },
            arrived: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            departed: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            trainId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
            stationId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
            entryDataId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
            createdAt: {
                type: Sequelize.DATE,
                allowNull: false,
            },
            updatedAt: {
                type: Sequelize.DATE,
                allowNull: false,
            }
        }).then(() => {
            queryInterface.addIndex('RouteEntries', ['trainId']);
            queryInterface.addIndex('RouteEntries', ['stationId']);
            queryInterface.addIndex('RouteEntries', ['entryDataId']);
            queryInterface.addIndex('RouteEntries', ['plannedArrival']);
            queryInterface.addIndex('RouteEntries', ['currentArrival']);
            queryInterface.addIndex('RouteEntries', ['plannedDeparture']);
            queryInterface.addIndex('RouteEntries', ['currentDeparture']);
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('RouteEntries');
    }
};