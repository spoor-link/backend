'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Trains', {
            id: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true,
                allowNull: false,
            },
            trainId: {
                type: Sequelize.STRING(45),
                allowNull: false,
            },
            trainNumber: {
                type: Sequelize.STRING(45),
                allowNull: false,
            },
            trainDate: {
                type: Sequelize.DATEONLY,
                allowNull: false,
            },
            status: {
                type: Sequelize.INTEGER,
                allowNull: false,
                defaultValue: 0,
            },
            track: {
                type: Sequelize.STRING(45),
                allowNull: true,
            },
            comments: {
                type: Sequelize.TEXT,
                allowNull: true,
            },
            plannedStartId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
            currentStartId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
            plannedDestinationId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
            currentDestinationId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
            companyId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
            trainTypeId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
            lat: {
                type: Sequelize.DOUBLE(10, 7),
                allowNull: true,
            },
            lon: {
                type: Sequelize.DOUBLE(10, 7),
                allowNull: true,
            },
            GPSAt: {
                type: Sequelize.DATE,
                allowNull: true,
            },
            prevTrainId: {
                type: Sequelize.UUID,
                allowNull: true,
            },
            nextTrainId: {
                type: Sequelize.UUID,
                allowNull: true,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        }).then(() => {
            queryInterface.addIndex('Trains', ['trainId']);
            queryInterface.addIndex('Trains', ['trainNumber']);
            queryInterface.addIndex('Trains', ['trainDate']);
            queryInterface.addIndex('Trains', ['companyId']);
            queryInterface.addIndex('Trains', ['trainTypeId']);
            queryInterface.addIndex('Trains', ['prevTrainId']);
            queryInterface.addIndex('Trains', ['nextTrainId']);
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Trains');
    }
};