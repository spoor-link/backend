'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Positions', {
            id: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true,
                allowNull: false,
            },
            lat: {
                type: Sequelize.DOUBLE(10, 7),
                allowNull: false,
            },
            lon: {
                type: Sequelize.DOUBLE(10, 7),
                allowNull: false,
            },
            speed: {
                type: Sequelize.DOUBLE(6, 3),
                allowNull: false,
            },
            direction: {
                type: Sequelize.DOUBLE(5, 2),
                allowNull: false,
            },
            trainId: {
                type: Sequelize.UUID,
                allowNull: true,
            },
            rollingStockId: {
                type: Sequelize.UUID,
                allowNull: true,
            },
            measuredAt: {
                type: Sequelize.DATE,
                allowNull: false,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        }).then(() => {
            queryInterface.addIndex('Positions', ['trainId']);
            queryInterface.addIndex('Positions', ['rollingStockId']);
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Positions');
    }
};