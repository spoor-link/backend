'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Companies', {
            id: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true,
                allowNull: false,
            },
            companyCode: {
                type: Sequelize.STRING,
            },
            code: {
                type: Sequelize.STRING,
            },
            fullName: {
                type: Sequelize.STRING,
            },
            turnOfDay: {
                type: Sequelize.STRING,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        }).then(() => {
            queryInterface.addIndex('Companies', ['companyCode']);
            queryInterface.addIndex('Companies', ['code']);
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Companies');
    }
};