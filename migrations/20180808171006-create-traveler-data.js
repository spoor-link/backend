'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('TravelerData', {
            id: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true,
                allowNull: false,
            },
            reservation: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            fee: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            noBoarding: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            rearTrainPartTerminates: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            shunting: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            specialTicket: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            replacementTransport: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            messages: {
                type: Sequelize.TEXT,
                allowNull: true,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('TravelerData');
    }
};