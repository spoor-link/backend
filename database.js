require('dotenv').config({
    path: './config/.env',
});

module.exports = {
    username: process.env.db_user,
    password: process.env.db_pass,
    database: process.env.db_database,
    host: process.env.db_host,
    dialect: 'mysql',
    define: {
        charset: 'utf8',
        collate: 'utf8_unicode_ci',
    },
    logging: false,
    operatorsAliases: false,
};