const Backend = require('./lib/Backend');
const path = require('path');
const find = require('find-process');

// Trigger the boot sequence
process.on('message', (m) => {
    // If the message contains the parent PID
    if(m.pid) {
        // Find information about the parent process
        find('pid', m.pid).then(list => {
            let argv = list[0].cmd.split(' ');

            let intendedParent = path.join(__dirname, 'backend.js');
            let actualParent = argv[1];

            // If the actual parent equals the intended parent
            if(intendedParent === actualParent) {
                process.env.TZ = process.env.timezone || 'Europe/Amsterdam';

                let backend = new Backend();
                backend.run();
            }
            else {
                console.error('This script should not be run standalone.');
                process.exit();
            }
        }, err => {
            console.log(err);
        });
    }
});