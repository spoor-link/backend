const fs = require('fs');
const path = require('path');
const fork = require('child_process').fork;

require('dotenv').config();

let cwd = __dirname;
let runFile = path.join(cwd, 'run.js');

let dotEnvPath = path.join(cwd, 'config', '.env');

console.log('Starting Spoor.link Backend server');
fs.exists(runFile, exists => {
    if(exists) {
        console.info('Starting backend process');
        let child = fork(runFile, [
            `dotenv_config_path=${dotEnvPath}`,
        ], {
            cwd,
            execArgv: [
                '--require',
                'dotenv/config',
            ]
        });

        child.send({
            pid: process.pid,
        });

        child.on('close', (code, signal) => {
            console.log(code);
            console.log(signal);
            process.exit(code);
        });
    }
    else {
        console.error(`Could not find file ${path.basename(runFile)}.`);
    }
});