'use strict';
module.exports = (sequelize, DataTypes) => {
    var RouteEntry = sequelize.define('RouteEntry', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            allowNull: false,
        },
        stopType: {
            type: DataTypes.ENUM('f', 's', 'p'),
            allowNull: false,
        },
        announced: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        cancelledArrival: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        cancelledDeparture: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        plannedArrival: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        currentArrival: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        plannedDeparture: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        currentDeparture: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        plannedTrack: {
            type: DataTypes.STRING(45),
            allowNull: true,
        },
        currentTrack: {
            type: DataTypes.STRING(45),
            allowNull: true,
        },
        arrived: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        departed: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        trainId: {
            type: DataTypes.UUID,
            allowNull: false,
        },
        stationId: {
            type: DataTypes.UUID,
            allowNull: false,
        },
        entryDataId: {
            type: DataTypes.UUID,
            allowNull: false,
        },
    }, {});
    RouteEntry.associate = function (models) {
        models.RouteEntry.belongsTo(models.TravelerData, {as: 'entryData', onDelete: 'cascade'});
        models.RouteEntry.belongsTo(models.Station, {as: 'station'});
        models.RouteEntry.belongsTo(models.Train);
        models.RouteEntry.belongsToMany(models.RollingStock, {
            through: 'RouteEntryHasRollingStock',
        })
    };
    return RouteEntry;
};