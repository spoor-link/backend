'use strict';
module.exports = (sequelize, DataTypes) => {
  var RouteEntryHasRollingStock = sequelize.define('RouteEntryHasRollingStock', {}, {});
  RouteEntryHasRollingStock.associate = function(models) {
    // associations can be defined here
  };
  return RouteEntryHasRollingStock;
};