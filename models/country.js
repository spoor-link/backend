'use strict';
module.exports = (sequelize, DataTypes) => {
    var Country = sequelize.define('Country', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            allowNull: false,
        },
        code: {
            type: DataTypes.STRING,
        },
        fullName: {
            type: DataTypes.STRING,
            get() {
                const fullName = this.getDataValue('fullName');
                // 'this' allows you to access attributes of the instance
                return Buffer.from(fullName, 'base64').toString();
            },
            set(val) {
                this.setDataValue('fullName', Buffer.from(val).toString('base64'));
            },
        },
        timezone: {
            type: DataTypes.STRING,
        },
    }, {
        charset: 'utf8',
    });
    Country.associate = function (models) {
        models.Country.hasMany(models.Station);
    };
    return Country;
};