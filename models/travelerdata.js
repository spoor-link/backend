'use strict';
module.exports = (sequelize, DataTypes) => {
    var TravelerData = sequelize.define('TravelerData', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            allowNull: false,
        },
        reservation: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        fee: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        noBoarding: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        rearTrainPartTerminates: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        shunting: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        specialTicket: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        replacementTransport: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        messages: {
            type: DataTypes.JSON,
            allowNull: true,
        },
    }, {});
    TravelerData.associate = function (models) {
        // associations can be defined here
    };
    return TravelerData;
};