'use strict';
module.exports = (sequelize, DataTypes) => {
    var Company = sequelize.define('Company', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            allowNull: false,
        },
        companyCode: {
            type: DataTypes.STRING,
        },
        code: {
            type: DataTypes.STRING,
        },
        fullName: {
            type: DataTypes.STRING,
            get() {
                const fullName = this.getDataValue('fullName');
                // 'this' allows you to access attributes of the instance
                return Buffer.from(fullName, 'base64').toString('ascii');
            },
            set(val) {
                this.setDataValue('fullName', Buffer.from(val).toString('base64'));
            },
        },
        turnOfDay: {
            type: DataTypes.STRING,
        },
    }, {});
    Company.associate = function (models) {
        models.Company.hasMany(models.Train);
    };
    return Company;
};