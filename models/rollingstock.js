'use strict';
module.exports = (sequelize, DataTypes) => {
    var RollingStock = sequelize.define('RollingStock', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            allowNull: false,
        },
        description: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        unitId: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        length: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        carriages: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        isGroup: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        familyId: {
            type: DataTypes.UUID,
            allowNull: true,
        },
    }, {});
    RollingStock.associate = function (models) {
        models.RollingStock.hasMany(models.Position);
        models.RollingStock.belongsToMany(models.RouteEntry, {
            through: 'RouteEntryHasRollingStock',
        })
    };
    return RollingStock;
};