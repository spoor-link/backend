'use strict';
module.exports = (sequelize, DataTypes) => {
    var Station = sequelize.define('Station', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            allowNull: false,
        },
        changeTime: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        lat: {
            type: DataTypes.DOUBLE(10, 7),
            allowNull: false,
        },
        lon: {
            type: DataTypes.DOUBLE(10, 7),
            allowNull: false,
        },
        code: {
            type: DataTypes.STRING(45),
            allowNull: false,
        },
        shortName: {
            type: DataTypes.STRING(45),
            allowNull: true,
            get() {
                const shortName = this.getDataValue('shortName');
                // 'this' allows you to access attributes of the instance
                return Buffer.from(shortName, 'base64').toString('ascii');
            },
            set(val) {
                this.setDataValue('shortName', Buffer.from(val).toString('base64'));
            },
        },
        middleName: {
            type: DataTypes.STRING(45),
            allowNull: true,
            get() {
                const middleName = this.getDataValue('middleName');
                // 'this' allows you to access attributes of the instance
                return Buffer.from(middleName, 'base64').toString('ascii');
            },
            set(val) {
                this.setDataValue('middleName', Buffer.from(val).toString('base64'));
            },
        },
        longName: {
            type: DataTypes.STRING(45),
            allowNull: false,
            get() {
                const longName = this.getDataValue('longName');
                // 'this' allows you to access attributes of the instance
                return Buffer.from(longName, 'base64').toString('ascii');
            },
            set(val) {
                this.setDataValue('longName', Buffer.from(val).toString('base64'));
            },
        },
        passengers: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true,
        },
        type: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        countryId: {
            type: DataTypes.UUID,
            allowNull: false,
        },
    }, {});
    Station.associate = function (models) {
        models.Station.hasMany(models.RouteEntry);
        models.Station.belongsTo(models.Country, {as: 'country'});
    };
    return Station;
};