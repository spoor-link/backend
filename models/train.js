'use strict';
module.exports = (sequelize, DataTypes) => {
    var Train = sequelize.define('Train', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            allowNull: false,
        },
        trainId: {
            type: DataTypes.STRING(45),
            allowNull: false,
        },
        trainNumber: {
            type: DataTypes.STRING(45),
            allowNull: false,
        },
        trainDate: {
            type: DataTypes.DATEONLY,
            allowNull: false,
        },
        status: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
        },
        track: {
            type: DataTypes.STRING(45),
            allowNull: true,
        },
        comments: {
            type: DataTypes.TEXT,
            allowNull: true,
        },
        plannedStartId: {
            type: DataTypes.UUID,
            allowNull: false,
        },
        currentStartId: {
            type: DataTypes.UUID,
            allowNull: false,
        },
        plannedDestinationId: {
            type: DataTypes.UUID,
            allowNull: false,
        },
        currentDestinationId: {
            type: DataTypes.UUID,
            allowNull: false,
        },
        companyId: {
            type: DataTypes.UUID,
            allowNull: false,
        },
        trainTypeId: {
            type: DataTypes.UUID,
            allowNull: false,
        },
        lat: {
            type: DataTypes.DOUBLE(10, 7),
            allowNull: true,
        },
        lon: {
            type: DataTypes.DOUBLE(10, 7),
            allowNull: true,
        },
        GPSAt: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        prevTrainId: {
            type: DataTypes.UUID,
            allowNull: true,
        },
        nextTrainId: {
            type: DataTypes.UUID,
            allowNull: true,
        },
    }, {});
    Train.associate = function (models) {
        models.Train.hasMany(models.RouteEntry, {as: 'routeEntries', onDelete: 'cascade'});
        models.Train.hasMany(models.Position, {as: 'positions', onDelete: 'cascade'});
        models.Train.belongsTo(models.Company, {as: 'company'});
        models.Train.belongsTo(models.TrainType, {as: 'trainType'});
        models.Train.belongsTo(models.Train, {
            as: 'PreviousTrain',
            foreignKey: 'trainId',
            sourceKey: 'prevTrainId',
        });
        models.Train.belongsTo(models.Train, {
            as: 'NextTrain',
            foreignKey: 'trainId',
            sourceKey: 'nextTrainId',
        });
    };
    return Train;
};