'use strict';
module.exports = (sequelize, DataTypes) => {
    const RollingStockFamily = sequelize.define('RollingStockFamily', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            allowNull: false,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    }, {});
    RollingStockFamily.associate = function (models) {
        // associations can be defined here
    };
    return RollingStockFamily;
};