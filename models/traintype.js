'use strict';
module.exports = (sequelize, DataTypes) => {
  var TrainType = sequelize.define('TrainType', {
      id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true,
          allowNull: false,
      },
      code: {
          type: DataTypes.STRING,
      },
      fullName: {
          type: DataTypes.STRING,
      },
      passengers: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          default: true,
      },
  }, {});
  TrainType.associate = function(models) {
    models.TrainType.hasMany(models.Train);
  };
  return TrainType;
};