'use strict';
module.exports = (sequelize, DataTypes) => {
  var Position = sequelize.define('Position', {
      id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true,
          allowNull: false,
      },
      lat: {
          type: DataTypes.DOUBLE(10, 7),
          allowNull: false,
      },
      lon: {
          type: DataTypes.DOUBLE(10, 7),
          allowNull: false,
      },
      speed: {
          type: DataTypes.DOUBLE(6, 3),
          allowNull: false,
      },
      direction: {
          type: DataTypes.DOUBLE(5, 2),
          allowNull: false,
      },
      trainId: {
          type: DataTypes.UUID,
          allowNull: true,
      },
      rollingStockId: {
          type: DataTypes.UUID,
          allowNull: true,
      },
      measuredAt: {
          type: DataTypes.DATE,
          allowNull: false,
      },
  }, {});
  Position.associate = function(models) {
    models.Position.belongsTo(models.Train);
    models.Position.belongsTo(models.RollingStock);
  };
  return Position;
};