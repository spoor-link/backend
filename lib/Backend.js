const async = require('async');
const os = require('os');
const {exec} = require('child_process');

const Processors = require('./Processors');
const Webserver = require('./Webserver');

/**
 * Main backend class. From here, the application is bootstrapped.
 * This class sets up the ORM and logging and starts the processors.
 * The processors are located in the ./processors folder. See ./Processors.js for more information.
 */
class Backend {
    constructor() {
        this.processors = null;
        this.webserver = null;

        this.ORM = null;
        this.logging = null;
    }

    /**
     * The only function that gets called from the outside.
     * This starts the application by registering the ORM and logging and initializing the processor boot.
     */
    run() {
        async.series(
            [
                callback => {
                    this._setDatabase(callback);
                },
                callback => {
                    this._setORM(callback);
                },
                callback => {
                    this._setLogging(callback);
                },
            ],
            err => {
                if (err) {
                    console.error(err);
                }
                else {
                    // After bootstrapping has been completed, start all processors and webserver
                    this._startApp();
                }
            });
    }

    /**
     * After all necessary stuff is available, start the app by starting the processors and the webserver.
     * @protected
     */
    _startApp() {
        async.series(
            [
                callback => {
                    this._runProcessors(callback);
                },
                callback => {
                    this._runWebserver(callback);
                },
                callback => {
                    this._sayHello(callback);
                },
            ]
        );
    }

    /**
     * This initializes the database itself by making sure all tables are present in the correct format.
     * @param callback
     * @protected
     */
    _setDatabase(callback) {
        exec('"node_modules/.bin/sequelize" db:migrate', (error, stdout, stderr) => {
            if (error) {
                console.error(`exec error: ${error}`);

                if (callback)
                    callback(error);
                return;
            }
            console.log(`stderr: ${stderr}`);

            if (callback)
                callback(null);
        });
    }

    /**
     * This initializes the ORM by calling the main script for the models.
     * It uses Sequelize as a database interaction ORM.
     * It also checks if all necessary ORM classes are present before continuing.
     * @param {Function} callback - The function to call when the process is completed
     * @protected
     */
    _setORM(callback) {
        try {
            this.ORM = require('../models');

            if (!this.ORM.Company ||
                !this.ORM.Country ||
                !this.ORM.Position ||
                !this.ORM.RollingStock ||
                !this.ORM.RouteEntry ||
                !this.ORM.Station ||
                !this.ORM.Train ||
                !this.ORM.TrainType ||
                !this.ORM.TravelerData)
                throw 'Essential models missing';

            callback(null);
        }
        catch (e) {
            callback(e);
        }

    }

    /**
     * This initializes the logging. This is used throughout the application for displaying and saving important information.
     * The logs are printed onto the console and saved to a date stamped file.
     * At the moment, these files are not deleted.
     * @param {Function} callback - The function to call when the process is completed
     * @protected
     */
    _setLogging(callback) {
        this.logging = require('./log');

        this.logging.general.info('Started backend.spoor.link server.');
        this.logging.general.info(`Platform:        ${os.type().replace('_', ' ')} ${os.release()}`);
        this.logging.general.info(`PID:             ${process.pid}`);
        this.logging.general.info(`NodeJS version:  ${process.version}`);
        this.logging.general.info('');

        callback();
    }

    /**
     * Start the processors.
     * Processors are designed to receive data and update the database.
     * They can also run timed tasks and/or do cleanup jobs.
     * @param {Function} callback - The function to call when the process is completed
     * @protected
     */
    _runProcessors(callback) {
        this.processors = new Processors();

        this.processors.addVariable('ORM', this.ORM);
        this.processors.addVariable('logging', this.logging);

        this.processors.addProcessors(() => {
            this.processors.preRun(() => {
                this.processors.run();

                if (callback)
                    callback();
            });
        });
    }

    /**
     * This starts the webserver from which the API is available.
     * @param {Function} callback - The function to call when the process is completed
     * @protected
     */
    _runWebserver(callback) {
        this.webserver = new Webserver();

        this.webserver.addVariable('ORM', this.ORM);
        this.webserver.addVariable('logging', this.logging);

        this.webserver.startServer(callback);
    }

    /**
     * A completely useless feature, which lets the user know the system is ready to go.
     * At the point this message is shown, all processors have completed their pre run and are running.
     * @protected
     */
    _sayHello() {
        // Uselessness: 100%

        try {
            const publicIp = require('public-ip');
            const dns = require('dns');

            this.logging.general.info('');
            this.logging.general.info('Spoor.link backend is booted up and ready to go!');
            this.logging.general.info('You should be able to reach the API on one of the following addresses:');
            this.logging.general.info(`http://127.0.0.1:${process.env.api_port}`);
            dns.lookup(os.hostname(), (err, address) => {
                this.logging.general.info(`http://${address}:${process.env.api_port}`);

                publicIp.v4().then(ip => {
                    this.logging.general.info(`http://${ip}:${process.env.api_port}`);

                    // Credits: https://asciiart.website/index.php?art=transportation/trains
                    this.logging.general.info('');
                    this.logging.general.info('      . . . . o o o o o');
                    this.logging.general.info('             _____      o        _______');
                    this.logging.general.info('    ____====  ]OO|_n_n__][.      |spoor|');
                    this.logging.general.info('   [________]_|__|________)<     | link|');
                    this.logging.general.info('    oo    oo  \'oo OOOO-| oo\\\\_   ~~~|~~~');
                    this.logging.general.info('+--+--+--+--+--+--+--+--+--+--+--+--+--+');
                    this.logging.general.info('');
                });
            });
        }
        catch (e) {
            // Pass
        }
    }
}

module.exports = Backend;