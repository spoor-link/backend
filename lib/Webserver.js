const http = require('http');
const express = require('express');
const WebSocket = require('ws');
const async = require('async');
const url = require('url');

const V1API = require('./webserver/APIs/V1/V1API');

/**
 * Runs the web API for the backend.
 * It delegates incoming REST calls and web socket connections to different versions of the API.
 * Each major API version has it's own class which gets a router object and incoming web socket connections are sent to the correct API.
 */
class Webserver {
    constructor() {
        this.variables = {};

        this.app = null; // Stores the ExpressJS instance
        this.http = null; // Stores the HTTP server instance

        this.APIs = []; // Stores all major API versions
    }

    /**
     * Adds a shared variable which can be used by the API versions
     * @param {String} key - The name of the variable
     * @param {Object} value - The value, can be any type
     */
    addVariable(key, value) {
        this.variables[key] = value;
    }

    /**
     * Start the web server. After this method is finished, the API is reachable
     * @param {Function=} callback - Called when the web server is started
     */
    startServer(callback) {
        this.variables.logging.general.info('Starting http webserver...');

        this.app = express();

        // Add logging
        this.app.use((req, res, next) => {
            this._logRequest(req, res, next);
        });

        this.app.get('/', (req, res) => {
            res.redirect('/v1');
        });

        this.http = http.createServer(this.app);
        this.app.server = this.http;

        this.addAPIs(() => {
            this.app.server.listen(process.env.api_port || 3000);

            if (callback)
                callback();
        });
    }

    /**
     * Define and add all available APIs. These don't have auto detection at the moment.
     * @param {Function=} callback - Called when all APIs are added
     */
    addAPIs(callback) {
        let APIs = [
            {
                class: V1API,
                httpBase: 'v1',
                socketBase: 'v1',
            },
        ];

        async.each(APIs, (API, callback) => {
            this.addAPI(API, callback);
        }, err => {
            if (err)
                this.variables.logging.general.error(err);

            if (callback)
                callback();
        });
    }

    /**
     * Add an API version. The API is given the shared variables, a route instance from ExpressJS and a handler for incoming web sockets.
     * @param {Object} API - The JSON object defining the API
     * @param {Object} API.class - The class of the API
     * @param {String} API.httpBase - The base URL for the HTTP server
     * @param {String} API.socketBase - The base URL for the web sockets
     * @param {Function=} callback - Called when the API is set up
     */
    addAPI(API, callback) {
        this.variables.logging.general.info(`Starting ${API.httpBase} API`);

        // Create a web socket server and a router to use
        let wss = new WebSocket.Server({noServer: true});
        let router = express.Router();

        // Handle the "upgrade" request from the server, meaning the client wants to setup a socket connection
        this.http.on('upgrade', (request, socket, head) => {
            let pathname = url.parse(request.url).pathname.replace(/\//g, '');

            if (pathname === API.socketBase) {
                wss.handleUpgrade(request, socket, head, ws => {
                    wss.emit('connection', ws, request);
                });
            }
        });

        this.app.use(`/${API.httpBase}`, router);

        // Pass the web socket server and router on towards the API version handler
        let APIInstance = new API.class(router, wss, this.variables);
        this.APIs.push(APIInstance);

        if (callback)
            callback();
    }

    /**
     * Logs an incoming request to the logging access file.
     * @param {Request} req - The web server Request object
     * @param {Response} res - The web server Response object
     * @param {Function} next - Callback for next middleware to run
     * @protected
     */
    _logRequest(req, res, next) {
        res.on('finish', () => {
            let logLine = `${req.ip}: ${req.method} ${res.statusCode} ${req.url}`;
            let statusGroup = res.statusCode.toString().substr(0, 1);

            switch (statusGroup) {
                case '4':
                    this.variables.logging.access.warn(logLine);
                    break;
                case '5':
                    this.variables.logging.access.error(logLine);
                    break;
                default:
                    this.variables.logging.access.info(logLine);
            }
        });

        next();
    }
}

module.exports = Webserver;