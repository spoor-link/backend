/**
 * Dijkstra's algorithm, conceived by Dutch computer scientist Edsger Dijkstra, is a graph search algorithm that solves the single-source shortest path problem for a graph with nonnegative edge path costs,producing a shortest path tree.
 * This algorithm is often used in routing and as a subroutine in other graph algorithms.
 */
class Graph {
    constructor(map) {
        this.map = map;
    }

    extractKeys(obj) {
        let keys = [], key;
        for (key in obj) {
            Object.prototype.hasOwnProperty.call(obj, key) && keys.push(key);
        }
        return keys;
    }

    sorter(a, b) {
        return parseFloat(a) - parseFloat(b);
    }

    findPaths(start, end, infinity) {
        infinity = infinity || Infinity;

        let costs = {},
            open = {'0': [start]},
            predecessors = {},
            keys;

        let addToOpen = (cost, vertex) => {
            let key = "" + cost;
            if (!open[key]) open[key] = [];
            open[key].push(vertex);
        }

        costs[start] = 0;

        while (open) {
            if (!(keys = this.extractKeys(open)).length) break;

            keys.sort(this.sorter);

            let key = keys[0],
                bucket = open[key],
                node = bucket.shift(),
                currentCost = parseFloat(key),
                adjacentNodes = this.map[node] || {};

            if (!bucket.length) delete open[key];

            for (let vertex in adjacentNodes) {
                if (Object.prototype.hasOwnProperty.call(adjacentNodes, vertex)) {
                    let cost = adjacentNodes[vertex],
                        totalCost = cost + currentCost,
                        vertexCost = costs[vertex];

                    if ((vertexCost === undefined) || (vertexCost > totalCost)) {
                        costs[vertex] = totalCost;
                        addToOpen(totalCost, vertex);
                        predecessors[vertex] = node;
                    }
                }
            }
        }

        if (costs[end] === undefined) {
            return null;
        } else {
            return predecessors;
        }

    }

    extractShortest(predecessors, end) {
        let nodes = [],
            u = end;

        while (u !== undefined) {
            nodes.push(u);
            u = predecessors[u];
        }

        nodes.reverse();
        return nodes;
    }

    findShortestPath(nodes) {
        let start = nodes.shift(),
            end,
            predecessors,
            path = [],
            shortest;

        while (nodes.length) {
            end = nodes.shift();
            predecessors = this.findPaths(start, end);

            if (predecessors) {
                shortest = this.extractShortest(predecessors, end);
                if (nodes.length) {
                    path.push.apply(path, shortest.slice(0, -1));
                } else {
                    return path.concat(shortest);
                }
            } else {
                return null;
            }

            start = end;
        }
    }

    toArray(list, offset) {
        try {
            return Array.prototype.slice.call(list, offset);
        } catch (e) {
            let a = [];
            for (let i = offset || 0, l = list.length; i < l; ++i) {
                a.push(list[i]);
            }
            return a;
        }
    }

    findShortestPathByStartAndEnd(start, end) {
        if (Object.prototype.toString.call(start) === '[object Array]') {
            return this.findShortestPath(start);
        } else if (arguments.length === 2) {
            return this.findShortestPath([start, end]);
        } else {
            return this.findShortestPath(this.toArray(arguments));
        }
    }
}

module.exports = {
    graph: new Graph(),
};