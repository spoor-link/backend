// log.js

const winston = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');
const fs = require('fs');
const path = require('path');

let logPath = path.join(process.cwd(), 'logs/node/');

if (!fs.existsSync(logPath)) {
    fs.mkdir(logPath);
}

let rotateFile = {
    json: false,
    localTime: true,
    zippedArchive: true,
    maxFiles: '30d',
}

rotateFile.filename = `${logPath}general.log`;
let general = new (winston.Logger)({
    level: process.env.log_level,
    transports: [
        new (winston.transports.Console)({
            json: false,
            timestamp: true,
            localTime: true,
        }),
        new DailyRotateFile(rotateFile),
    ],
    exceptionHandlers: [
        new (winston.transports.Console)({
            json: false,
            timestamp: true,
            localTime: true,
        }),
        new DailyRotateFile(rotateFile),
    ],
    exitOnError: false
});

rotateFile.filename = `${logPath}input.log`;
let input = new (winston.Logger)({
    level: process.env.log_level,
    transports: [
        new (winston.transports.Console)({
            json: false,
            timestamp: true,
            localTime: true,
        }),
        new DailyRotateFile(rotateFile),
    ],
    exitOnError: false
});

rotateFile.filename = `${logPath}access.log`;
let access = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({
            json: false,
            timestamp: true,
            localTime: true,
        }),
        new DailyRotateFile(rotateFile),
    ],
    exitOnError: false
});

module.exports = {
    general,
    input,
    access,
};