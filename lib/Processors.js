const async = require('async');
const fs = require('fs');
const path = require('path');
const schedule = require('node-schedule');
const moment = require('moment');

/**
 * Manages the application processors.
 * These processors are located in ./processors/
 *
 * Processors are automatically detected by reading through the directory and looking for *.js files.
 * This lookup is non-recursive, so you can add folders in there for more advanced processors that need multiple classes.
 *
 * Each processor has a Processor.getValidation() method. This method lets Processors know whether it can run or not.
 * The Processor.preRun() method of each processor is called before any Processor.run() methods get called.
 * This is to make sure all tasks required to do before the application can start are completed.
 *
 * You are not required to have any of the .getValidation(), .preRun() or .run() methods.
 * You are required to extend the Processor class.
 */
class Processors {
    constructor() {
        this.variables = {};
        this.processors = [];
        this.schedules = [];
    }

    /**
     * Add a variable which will be passed onto the processors
     * @param key the variable name
     * @param value it's value
     */
    addVariable(key, value) {
        this.variables[key] = value;
    }

    /**
     * This method will search for all processors and add them to the list.
     * The only requirement is that the filename ends with .js
     * @param callback
     */
    addProcessors(callback) {
        this.variables.logging.general.info('Searching for processors...');

        let processorsPath = path.join(__dirname, 'processors');

        fs.readdir(processorsPath, (err, files) => {
            async.each(files, (file, eachCallback) => {
                if (file.endsWith('.js')) {
                    file = path.join(processorsPath, file);

                    try {
                        if (fs.lstatSync(file).isFile()) {
                            let Processor = require(file);

                            this.variables.logging.general.info(`Found processor ${Processor.name}`);

                            this._addProcessor(Processor, () => {
                                eachCallback();
                            });
                        }
                        else {
                            eachCallback();
                        }
                    }
                    catch (e) {
                        this.variables.logging.general.error(`Error while processing processor file ${path.basename(file)}`);
                        this.variables.logging.general.error(e);
                    }
                }
                else {
                    eachCallback();
                }
            }, err => {
                if (err) {
                    this.variables.logging.general.error(err);
                }

                if (callback)
                    callback();
            });
        });
    }

    /**
     * Execute the pre run of all processors.
     * By default, their pre run won't do anything.
     * @param callback
     */
    preRun(callback) {
        this.variables.logging.general.info('Executing processor pre runs');
        async.eachSeries(this.processors, (processor, callback) => {
            if (typeof processor.preRun === 'function') {
                this.variables.logging.general.info(`Running pre run of ${processor.constructor.name}`);

                try {
                    processor.preRun(callback);
                }
                catch(e) {
                    this.variables.logging.general.error(`Error while executing pre run for ${processor.constructor.name}`);
                    this.variables.logging.general.error(e);

                    callback();
                }
            }
            else {
                callback();
            }
        }, err => {
            if (err) {
                this.variables.logging.general.error(err);
            }

            callback();
        });
    }

    /**
     * Start the main run of the processors.
     */
    run() {
        this.variables.logging.general.info('Starting processors main run');
        // Start each input
        if (this.processors.length !== 0) {
            this.processors.forEach(processor => {
                try {
                    // Start the processor
                    if (typeof processor.run === 'function') {
                        this.variables.logging.general.info(`Run processor ${processor.constructor.name}`);
                        processor.run();
                    }

                    // Check for scheduled tasks
                    if (typeof processor.runSchedule === 'function') {
                        processor.getSchedules().forEach(scheduleFormat => {
                            this.schedules.push(
                                schedule.scheduleJob(scheduleFormat, (fireDate) => {
                                    processor.runSchedule(moment(fireDate));
                                })
                            );
                        });
                    }
                }
                catch(e) {
                    this.variables.logging.general.error(`Error while starting processor ${processor.constructor.name}`);
                    this.variables.logging.general.error(e);
                }
            });
        }
        else {
            this.variables.logging.general.error('No processors to run');
        }
    }

    /**
     * Add a single processor to the processors stack.
     * It initializes the processor and adds the shared variables with it.
     * At the moment, there is no validation to make sure it's an actual processor, so be careful.
     * @param Processor
     * @param callback
     * @private
     */
    _addProcessor(Processor, callback) {
        let processor = new Processor();
        processor.setVariables(this.variables);

        if (processor.getValidation()) {
            this.processors.push(processor);
        }

        if (callback)
            callback();
    }
}

module.exports = Processors;