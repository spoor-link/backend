const readLine = require('readline');
const fs = require('fs');

/**
 * Base class for all IFF readers.
 */
class IFFFile {
    constructor(opts) {
        if(!opts)
            opts = {};

        this.opts = opts;
        this.return = [];
    }

    /**
     * Read and parse the specified file
     * @param {String} filePath - The (absolute) path of the IFF file
     * @param {readFileCallback|Null} callback
     */
    readFile(filePath, callback) {
        if (typeof this.parseLine !== 'undefined') {
            let lineReader = readLine.createInterface(
                {
                    input: fs.createReadStream(filePath, {
                        encoding: 'latin1'
                    }),
                }
            );

            lineReader.on('line', line => {
                lineReader.pause();
                this.parseLine(line, () => {
                    lineReader.resume();
                });
            });
            lineReader.on('close', () => {
                if(typeof this.finalizeReturn !== 'undefined') {
                    this.finalizeReturn(() => {
                        callback(this.return);
                    });
                }
                else {
                    if (callback)
                        callback(this.return);
                }
            });
        }
        else {
            if (callback)
                callback(this.return);
        }
    }

    /**
     * Split IFF data from a string into it's correct components. It applies a mapping to them, so every entry has a readable name.
     * @param {String} str - The input string
     * @param {String[]} mapping - A list of all mapping positions
     * @return {String[]} returnData - The trimmed data contained in an array
     */
    splitData(str, mapping) {
        let data = str.split(',');
        let returnData = {};

        let index;
        for (index = 0; index < data.length; ++index) {
            if (typeof mapping[index] !== 'undefined')
                returnData[mapping[index]] = data[index].trim();
            else
                returnData[index] = data[index].trim();
        }

        return returnData;
    }
}

/**
 * @callback readFileCallback
 * @param {Object} data - Parsed data from the IFF file
 */

module.exports = IFFFile;