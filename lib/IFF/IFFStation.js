const IFFFile = require('./IFFFile');

/**
 * Reads and parses stations from the IFF standard.
 */
class IFFStation extends IFFFile {
    constructor(opts) {
        super(opts);

        this.stationStartDate = null;
        this.stationEndDate = null;
    }

    /**
     * Parse a single line from the file. Called from IFFFile.readFile.
     * @param {String} line - One line from the file
     * @param {Function} callback - Called when it is done reading this line
     */
    parseLine(line, callback) {
        let command = line.substr(0, 1);
        let data = line.substr(1);

        switch (command) {
            case '@': // Start record
                this.parseStart(data, callback);
                break;
            default:
                this.parseStation(line, callback);
                break;
        }
    }

    /**
     * Parse the start of the file. This contains general information.
     * @param {String} data - Raw data without the command identifier
     * @param {Function=} callback - Called when the start entry is parsed
     */
    parseStart(data, callback) {
        let parsedData = this.splitData(data, [
            'company',
            'startDate',
            'endDate',
            'version',
            'description',
        ]);

        this.stationStartDate = parsedData.startDate;
        this.stationEndDate = parsedData.endDate;

        if (callback)
            callback();
    }

    /**
     * Parse a single station. These are not preceded by a command identifier. The file contains 1 station per line.
     * @param {String} data - The raw station data
     * @param {Function=} callback - Called when the station is parsed
     */
    parseStation(data, callback) {
        let parsedData = this.splitData(data, [
            'trainChanges',
            'code',
            'changeTime',
            'maxChangeTime',
            'countryCode',
            'timeZone',
            'attribute',
            'x',
            'y',
            'fullName',
        ]);

        delete parsedData.maxChangeTime;
        delete parsedData.attribute;

        parsedData.code = parsedData.code.toUpperCase();
        parsedData.trainChanges = parseInt(parsedData.trainChanges);
        parsedData.changeTime = parseInt(parsedData.changeTime);
        parsedData.x = parseFloat(parsedData.x) * 10; // Times 10 because for some weird reason NS decided to save their coordinates as RD / 10
        parsedData.y = parseFloat(parsedData.y) * 10;

        this.return.push(parsedData);

        if (callback)
            callback();
    }
}

module.exports = IFFStation;