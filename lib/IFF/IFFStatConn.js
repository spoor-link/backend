const IFFFile = require('./IFFFile');

/**
 * Reads and parses station connections from the IFF standard.
 *
 * @example
 * >ahp,ah
 * >ah,ahp
 * >ah,ahz
 * >ah,otb
 */
class IFFStatConn extends IFFFile {
    constructor(opts) {
        super(opts);
    }

    /**
     * Parse a single line from the file. Called from IFFFile.readFile.
     * @param {String} line - One line from the file
     * @param {Function} callback - Called when it is done reading this line
     */
    parseLine(line, callback) {
        let command = line.substr(0, 1);
        let data = line.substr(1);

        switch (command) {
            case '>': // Start record
                this.parseStatConn(data, callback);
                break;
        }
    }

    /**
     * Parse a single station connection. These connections are explicitly defined both ways in the file.
     * So if "AH" has a connection with "AHP", there will also be a connection from "AHP" to "AH".
     * @param {String} data - the raw line data without the command identifier
     * @param {Function=} callback - Called when the line is imported
     */
    parseStatConn(data, callback) {
        let parsedData = this.splitData(data, [
            'from',
            'to',
        ]);

        parsedData.from = parsedData.from.toUpperCase();
        parsedData.to = parsedData.to.toUpperCase();

        this.return.push(parsedData);

        if (callback)
            callback();
    }
}

module.exports = IFFStatConn;