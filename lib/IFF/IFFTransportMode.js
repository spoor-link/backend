const IFFFile = require('./IFFFile');

/**
 * Reads and parses transport modes from the IFF standard.
 */
class IFFTransportMode extends IFFFile {
    constructor(opts) {
        super(opts);

        this.transportModeStartDate = null;
        this.transportModeEndDate = null;
    }

    /**
     * Parse a single line from the file. Called from IFFFile.readFile.
     * @param {String} line - One line from the file
     * @param {Function} callback - Called when it is done reading this line
     */
    parseLine(line, callback) {
        let command = line.substr(0, 1);
        let data = line.substr(1);

        switch (command) {
            case '@': // Start record
                this.parseStart(data, callback);
                break;
            default:
                this.parseTransportMode(line, callback);
                break;
        }
    }

    /**
     * Parse the start of the file. This contains general information.
     * @param {String} data - Raw data without the command identifier
     * @param {Function=} callback - Called when the start entry is parsed
     */
    parseStart(data, callback) {
        let parsedData = this.splitData(data, [
            'company',
            'startDate',
            'endDate',
            'version',
            'description',
        ]);

        this.transportModeStartDate = parsedData.startDate;
        this.transportModeEndDate = parsedData.endDate;

        if (callback)
            callback();
    }

    /**
     * Parse a single transport mode. These are not preceded by a command identifier. The file contains 1 transport mode per line.
     * @param {String} data - The raw transport mode data
     * @param {Function=} callback - Called when the transport mode is parsed
     */
    parseTransportMode(data, callback) {
        let parsedData = this.splitData(data, [
            'code',
            'fullName',
        ]);

        parsedData.passengers = true;

        this.return.push(parsedData);

        if (callback)
            callback();
    }
}

module.exports = IFFTransportMode;