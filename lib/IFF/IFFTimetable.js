const async = require('async');
const moment = require('moment');
const IFFFile = require('./IFFFile');

/**
 * Reads and parses the timetable from the IFF standard.
 * This is the most complex file within IFF. It contains all trains in the entire timetable, which typically is defined for one year.
 */
class IFFTimetable extends IFFFile {
    constructor(opts) {
        super(opts);

        // Set the footnote filter
        if (!this.opts.footnoteFilter)
            this.opts.footnoteFilter = [];

        // Set the timetable date
        if (!this.opts.date)
            this.opts.date = moment();
        else
            this.opts.date = moment(this.opts.date);
        if (!this.opts.date.isValid())
            throw 'No valid date';

        // Make sure we only have the date left
        this.opts.date.millisecond(0);
        this.opts.date.second(0);
        this.opts.date.minute(0);
        this.opts.date.hour(0);

        this.currentTrain = {};
        this.timetableStartDate = null;
        this.timetableEndDate = null;
    }

    /**
     * Parse a single line from the file. Called from IFFFile.readFile.
     * @param {String} line - One line from the file
     * @param {Function} callback - Called when it is done reading this line
     */
    parseLine(line, callback) {
        let command = line.substr(0, 1);
        let data = line.substr(1);

        switch (command) {
            case '@': // Start record
                this.parseStart(data, callback);
                break;
            case '#': // Service ID record, save previous train
                this.parseServiceID(data, callback);
                break;
            case '%': // Service number record
                this.parseServiceNumber(data, callback);
                break;
            case '-': // Footnote record
                this.parseFootnote(data, callback);
                break;
            case '&': // Train type record
                this.parseTrainType(data, callback);
                break;
            case '*': // Train attribute
                this.parseTrainAttribute(data, callback);
                break;
            case '>': // Start station record
                this.parseStartStation(data, callback);
                break;
            case '?': // Track record
                this.parseTrack(data, callback);
                break;
            case ';': // Pass station record
                this.parsePassStation(data, callback);
                break;
            case '.': // Continuation record
                this.parseContinuation(data, callback);
                break;
            case '+': // Interval record
                this.parseInterval(data, callback);
                break;
            case '<': // Final station record
                this.parseFinalStation(data, callback);
                break;
            default:
                console.log(`Unrecognized command: ${line}`);
                callback();
        }
    }

    /**
     * Parse the start of the file. This contains general information.
     * @param {String} data - Raw data without the command identifier
     * @param {Function=} callback - Called when the start entry is parsed
     */
    parseStart(data, callback) {
        let parsedData = this.splitData(data, [
            'company',
            'startDate',
            'endDate',
            'version',
            'description',
        ]);
        this.timetableStartDate = parsedData.startDate;
        this.timetableEndDate = parsedData.endDate;

        if (callback)
            callback();
    }

    /**
     * Parse the service ID. This is the unique identifier of a train.
     * This ID has nothing to do with the train number, because that can be defined multiple times for different days.
     * The ID also signals the start of a new train, so the current train is saved, and a new skeleton is created.
     * @param {String} data - The data containing the service ID
     * @param {Function=} callback - Called when the train is saved and the ID is parsed
     */
    parseServiceID(data, callback) {
        this.saveTrain(() => {
            let parsedData = this.splitData(data, [
                'ID',
            ]);

            this.currentTrain.id = parsedData.ID;

            if (callback)
                callback();
        });
    }

    /**
     * Parse the service number. This is the train number the train will carry.
     * There can be multiple service numbers, as trains can renumber en-route.
     * @param {String} data - Raw data without the command identifier
     * @param {Function=} callback - Called when the service number is parsed
     */
    parseServiceNumber(data, callback) {
        if (this.currentTrain.ignore === false) {
            let parsedData = this.splitData(data, [
                'company',
                'trainNumber',
                'lineNumber',
                'startStation',
                'endStation',
                'trainName',
            ]);

            if (parsedData.lineNumber)
                parsedData.trainNumber = parsedData.lineNumber;

            if (parsedData.startStation)
                parsedData.startStation = parseInt(parsedData.startStation) - 1; // Subtract 1 to make the array zero-based

            if (parsedData.endStation)
                parsedData.endStation = parseInt(parsedData.endStation) - 1;

            delete parsedData.lineNumber;

            // Remove all leading zeroes from the train number
            parsedData.trainNumber = parsedData.trainNumber.replace(/^[0.]+/, "");

            if (typeof this.currentTrain.trainNumbers === 'undefined')
                this.currentTrain.trainNumbers = [];

            this.currentTrain.trainNumbers.push(parsedData);
        }

        if (callback)
            callback();
    }

    /**
     * Parse a footnote which belongs to the train.
     * These footnotes define on which days the train will be available.
     * A train can have multiple footnotes, but it's very rare for trains to have multiple footnotes.
     * @param {String} data - Raw data without the command identifier
     * @param {Function=} callback - Called when the footnote is parsed
     */
    parseFootnote(data, callback) {
        if (this.currentTrain.ignore === false) {
            let parsedData = this.splitData(data, [
                'footnote',
                'startStation',
                'endStation',
            ]);

            if (this.opts.footnoteFilter.length !== 0 && !this.opts.footnoteFilter.includes(parsedData.footnote))
                this.currentTrain.ignore = true;
            else
                this.currentTrain.footnote = parsedData.footnote;
        }

        if (callback)
            callback();
    }

    /**
     * Parse a train type which belongs to the train.
     * Train types are types like: intercity, stopper train, etc...
     * A train can have multiple train types.
     * @param {String} data - Raw data without the command identifier
     * @param {Function=} callback - Called when the train type is parsed
     */
    parseTrainType(data, callback) {
        if (this.currentTrain.ignore === false) {
            let parsedData = this.splitData(data, [
                'trainType',
                'startStation',
                'endStation',
            ]);

            if (parsedData.startStation)
                parsedData.startStation = parseInt(parsedData.startStation) - 1; // Subtract 1 to make the array zero-based

            if (parsedData.endStation)
                parsedData.endStation = parseInt(parsedData.endStation) - 1;

            if (typeof this.currentTrain.trainTypes === 'undefined')
                this.currentTrain.trainTypes = [];

            this.currentTrain.trainTypes.push(parsedData);
        }

        if (callback)
            callback();
    }

    /**
     * Parse an attribute of the train.
     * Attributes are usually passenger information. This can be various things like: bikes not allowed, extra fees and restaurants on board.
     * @param {String} data - Raw data without the command identifier
     * @param {Function=} callback - Called when the attribute is parsed
     */
    parseTrainAttribute(data, callback) {
        if (this.currentTrain.ignore === false) {
            let parsedData = this.splitData(data, [
                'attribute',
                'startStation',
                'endStation',
                'footnote',
            ]);

            if (parsedData.startStation)
                parsedData.startStation = parseInt(parsedData.startStation) - 1; // Subtract 1 to make the array zero-based

            if (parsedData.endStation)
                parsedData.endStation = parseInt(parsedData.endStation) - 1;

            if (typeof this.currentTrain.attributes === 'undefined')
                this.currentTrain.attributes = [];

            this.currentTrain.attributes.push(parsedData);
        }

        if (callback)
            callback();
    }

    /**
     * Parse the starting station of the train.
     * @param {String} data - Raw data without the command identifier
     * @param {Function=} callback - Called when the start station is parsed
     */
    parseStartStation(data, callback) {
        if (this.currentTrain.ignore === false) {
            let parsedData = this.splitData(data, [
                'station',
                'departure',
            ]);

            parsedData.station = parsedData.station.toUpperCase();
            parsedData.arrival = null;
            parsedData.departure = this.parseTime(parsedData.departure);
            parsedData.stopType = 'f';

            if (typeof this.currentTrain.routeEntries === 'undefined')
                this.currentTrain.routeEntries = [];

            this.currentTrain.routeEntries.push(parsedData);
        }

        if (callback)
            callback();
    }

    /**
     * Parse a track assignment.
     * It belongs to a top and will be provided directly after one.
     * @param {String} data - Raw data without the command identifier
     * @param {Function=} callback - Called when the track is parsed
     */
    parseTrack(data, callback) {
        if (this.currentTrain.ignore === false) {
            let parsedData = this.splitData(data, [
                'arrivalTrack',
                'departureTrack',
                'footnote',
            ]);

            let track = parsedData.arrivalTrack;
            if (track.length < parsedData.departureTrack.length)
                track = parsedData.departureTrack;

            let lastRouteEntryIndex = this.currentTrain.routeEntries.length - 1;

            if (lastRouteEntryIndex >= 0)
                this.currentTrain.routeEntries[lastRouteEntryIndex].track = track;
        }

        if (callback)
            callback();
    }

    /**
     * Parse a pass station.
     * These are stations which the train passes, but it doesn't stop.
     * Currently, these stations are not processed, because they are a royal nightmare to work with.
     * They don't count for all startStation and endStation declarations.
     * @param {String} data - Raw data without the command identifier
     * @param {Function=} callback - Called when the pass station is parsed
     */
    parsePassStation(data, callback) {
        if (this.currentTrain.ignore === false) {
            let parsedData = this.splitData(data, [
                'station',
            ]);

            parsedData.station = parsedData.station.toUpperCase();
            parsedData.arrival = null;
            parsedData.departure = null;
            parsedData.stopType = 'p';

            if (typeof this.currentTrain.routeEntries === 'undefined')
                this.currentTrain.routeEntries = [];

            // These things are annoying and stupid, so we're ignoring them
            // TODO: Fix this so it works
            //this.currentTrain.routeEntries.push(data);
        }

        if (callback)
            callback();
    }

    /**
     * Parse a continuation.
     * These are stations where the train stops and departs again in the same minute.
     * Hence, it only has a departure time
     * @param {String} data - Raw data without the command identifier
     * @param {Function=} callback - Called when the continuation is parsed
     */
    parseContinuation(data, callback) {
        if (this.currentTrain.ignore === false) {
            let parsedData = this.splitData(data, [
                'station',
                'departure',
            ]);

            parsedData.station = parsedData.station.toUpperCase();
            parsedData.arrival = null;
            parsedData.departure = this.parseTime(parsedData.departure);
            parsedData.stopType = 's';

            if (typeof this.currentTrain.routeEntries === 'undefined')
                this.currentTrain.routeEntries = [];

            this.currentTrain.routeEntries.push(parsedData);
        }

        if (callback)
            callback();
    }

    /**
     * Parse an interval.
     * These are stations where the train stops, and the arrival and departure times are different from each other.
     * @param {String} data - Raw data without the command identifier
     * @param {Function=} callback - Called when the interval is parsed
     */
    parseInterval(data, callback) {
        if (this.currentTrain.ignore === false) {
            let parsedData = this.splitData(data, [
                'station',
                'arrival',
                'departure',
            ]);

            parsedData.station = parsedData.station.toUpperCase();
            parsedData.arrival = this.parseTime(parsedData.arrival);
            parsedData.departure = this.parseTime(parsedData.departure);
            parsedData.stopType = 'f';

            if (typeof this.currentTrain.routeEntries === 'undefined')
                this.currentTrain.routeEntries = [];

            this.currentTrain.routeEntries.push(parsedData);
        }

        if (callback)
            callback();
    }

    /**
     * Parse the final station.
     * Because this is it's terminal station, there is no departure time available.
     * @param {String} data - Raw data without the command identifier
     * @param {Function=} callback - Called when the final station is parsed
     */
    parseFinalStation(data, callback) {
        if (this.currentTrain.ignore === false) {
            let parsedData = this.splitData(data, [
                'station',
                'arrival',
            ]);

            parsedData.station = parsedData.station.toUpperCase();
            parsedData.arrival = this.parseTime(parsedData.arrival);
            parsedData.departure = null;
            parsedData.stopType = 'f';

            if (typeof this.currentTrain.routeEntries === 'undefined')
                this.currentTrain.routeEntries = [];

            this.currentTrain.routeEntries.push(parsedData);
        }

        if (callback)
            callback();
    }

    /**
     * Save the current train to the return array.
     * It also processes the current train. The train gets split into multiple trains with only a single train number.
     * @param {Function} callback - Called when the train is processed and saved
     */
    saveTrain(callback) {
        // If there is a train in the variable
        if (typeof this.currentTrain.id !== 'undefined' && this.currentTrain.ignore === false) {
            delete this.currentTrain.ignore;

            /*
             * Loop through each train number to make separate any trains that have multiple
             */
            async.each(this.currentTrain.trainNumbers, (trainNumber, eachCallback) => {
                let returnTrain = {
                    trainNumber,
                    footnote: this.currentTrain.footnote,
                    routeEntries: this.currentTrain.routeEntries.slice(trainNumber.startStation, trainNumber.endStation + 1),
                    trainTypes: [],
                    attributes: [],
                };

                async.series([
                        /*
                         * Attach the right train types
                         */
                        (seriesCallback) => {
                            if (typeof this.currentTrain.trainTypes !== 'undefined') {
                                async.each(this.currentTrain.trainTypes, (trainType, trainTypeCallback) => {
                                    // Correct the train type's start and end bases on the train number starting station
                                    trainType.startStation = trainType.startStation - trainNumber.startStation;
                                    trainType.endStation = trainType.endStation - trainNumber.startStation;

                                    // Only add the train type if it goes beyond the first station
                                    if (trainType.endStation > 0) {
                                        trainType.startStation = Math.max(trainType.startStation, 0); // Set the startStation to a minimum of 0

                                        returnTrain.trainTypes.push(trainType);
                                    }
                                    trainTypeCallback();
                                }, (err) => {
                                    if (err)
                                        console.error(err);

                                    seriesCallback();
                                });
                            }
                            else {
                                seriesCallback();
                            }
                        },

                        /*
                         * Attach the right train attributes
                         */
                        (seriesCallback) => {
                            if (typeof this.currentTrain.attributes !== 'undefined') {
                                async.each(this.currentTrain.attributes, (trainAttribute, trainAttributeCallback) => {
                                    // Correct the train attribute's start and end bases on the train number starting station
                                    trainAttribute.startStation = trainAttribute.startStation - trainNumber.startStation;
                                    trainAttribute.endStation = trainAttribute.endStation - trainNumber.startStation;

                                    // Only add the train attribute if it goes beyond the first station
                                    if (trainAttribute.endStation > 0) {
                                        trainAttribute.startStation = Math.max(trainAttribute.startStation, 0); // Set the startStation to a minimum of 0

                                        returnTrain.attributes.push(trainAttribute);
                                    }
                                    trainAttributeCallback();
                                }, (err) => {
                                    if (err)
                                        console.error(err);

                                    seriesCallback();
                                });
                            }
                            else {
                                seriesCallback();
                            }
                        },
                    ],
                    /*
                     * After processing is complete, display any errors and add the parsed train to the list
                     */
                    (err) => {
                        if (err)
                            console.error(err);

                        this.return.push(returnTrain);

                        eachCallback(); // Start with the next train number
                    })
            }, (err) => {
                if (err)
                    console.error(err);

                this.currentTrain = {
                    ignore: false
                };
                callback();
            });
        }
        else {
            this.currentTrain = {
                ignore: false
            };
            callback();
        }

    }

    /**
     * Parse a time provided by the timetable.
     * These times are provided in the following format: 2100.
     * It is combined with the date given in the options to create a datetime Moment.
     * @param {String} timeString - The raw time
     * @return {moment.Moment} returnDate - the Moment instance of the given time
     */
    parseTime(timeString) {
        let returnDate = moment(this.opts.date);
        returnDate.hour(parseInt(timeString.substr(0, 2)));
        returnDate.minute(parseInt(timeString.substr(2, 2)));

        return returnDate;
    }
}

module.exports = IFFTimetable;