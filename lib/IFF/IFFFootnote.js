const IFFFile = require('./IFFFile');
const moment = require('moment');

/**
 * Reads and parses footnotes from the IFF standard.
 * Footnotes determine on which days trains will be available in the timetable.
 * Each footnote has an identifier and a string of booleans. One for each day in the timetable.
 * These booleans are presented by a 0 or a 1. If it's a 1, the train will be available for that day.
 * The booleans start from the first day if the timetable up to and including the last day.
 */
class IFFFootnote extends IFFFile {
    constructor(opts) {
        super(opts);

        this.currentFootnote = {};
        this.footnoteStartDate = null;
        this.footnoteEndDate = null;
        this.footnoteCheckIndex = null;
    }

    /**
     * Parse a single line from the file. Called from IFFFile.readFile.
     * @param {String} line - One line from the file
     * @param {Function} callback - Called when it is done reading this line
     */
    parseLine(line, callback) {
        let command = line.substr(0, 1);
        let data = line.substr(1);

        switch(command) {
            case '@': // Start record
                this.parseStart(data, callback);
                break;
            case '#': // Footnote ID
                this.parseID(data, callback);
                break;
            default:
                this.parseFootnote(line, callback);
                break;
        }
    }

    /**
     * Parse the start of the file. This contains general information.
     * @param {String} data - Raw data without the command identifier
     * @param {Function=} callback - Called when the start entry is parsed
     */
    parseStart(data, callback) {
        let parsedData = this.splitData(data, [
            'company',
            'startDate',
            'endDate',
            'version',
            'description',
        ]);

        this.footnoteStartDate = parsedData.startDate;
        this.footnoteEndDate = parsedData.endDate;

        if(this.opts.filterDate) {
            let startDate = moment(parsedData.startDate, 'DDMMYYYY');
            this.footnoteCheckIndex = moment(this.opts.filterDate).diff(startDate, 'days');
        }

        if(callback)
            callback();
    }

    /**
     * Parse the footnote ID. This ID is used by the timetable to get the correct footnote.
     * @param {String} ID - the footnote ID
     * @param {Function=} callback - Called when the ID is parsed
     */
    parseID(ID, callback) {
        this.currentFootnote.ID = ID;

        if(callback)
            callback();
    }

    /**
     * Parse the footnote. These are not preceded with a command character.
     * @param {String} line - The footnote
     * @param {Function=} callback - Called when the footnote is parsed and saved
     */
    parseFootnote(line, callback) {
        if(this.footnoteCheckIndex !== null) {
            let checkDay = line.substr(this.footnoteCheckIndex, 1);

            if(checkDay === '1') {
                this.currentFootnote.footnote = line;
                this.saveFootnote(callback);
            }
            else {
                this.currentFootnote = {};
                callback();
            }
        }
        else {
            this.currentFootnote.footnote = line;
            this.saveFootnote(callback);
        }
    }

    /**
     * Saves the footnote to the array which will be returned when all footnotes are parsed.
     * @param {Function=} callback - Called when the footnote is saved to the array
     */
    saveFootnote(callback) {
        this.return.push(this.currentFootnote);
        this.currentFootnote = {};

        if(callback)
            callback();
    }
}

module.exports = IFFFootnote;