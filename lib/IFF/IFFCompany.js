const IFFFile = require('./IFFFile');

/**
 * Reads and parses companies from the IFF standard.
 */
class IFFCompany extends IFFFile {
    constructor(opts) {
        super(opts);

        this.companyStartDate = null;
        this.companyEndDate = null;
    }

    /**
     * Parse a single line from the file. Called from IFFFile.readFile.
     * @param {String} line - One line from the file
     * @param {Function} callback - Called when it is done reading this line
     */
    parseLine(line, callback) {
        let command = line.substr(0, 1);
        let data = line.substr(1);

        switch (command) {
            case '@': // Start record
                this.parseStart(data, callback);
                break;
            default:
                this.parseCompany(line, callback);
                break;
        }
    }

    /**
     * Parse the start of the file. This contains general information.
     * @param {String} data - Raw data without the command identifier
     * @param {Function=} callback - Called when the start entry is parsed
     */
    parseStart(data, callback) {
        let parsedData = this.splitData(data, [
            'company',
            'startDate',
            'endDate',
            'version',
            'description',
        ]);

        this.companyStartDate = parsedData.startDate;
        this.companyEndDate = parsedData.endDate;

        if (callback)
            callback();
    }

    /**
     * Parse a single company. These are not preceded by a command identifier. The file contains 1 company per line.
     * @param {String} data - The raw company data
     * @param {Function=} callback - Called when the company is parsed
     */
    parseCompany(data, callback) {
        let parsedData = this.splitData(data, [
            'ID',
            'code',
            'fullName',
            'turnOfDay',
        ]);

        parsedData.turnOfDay = `${parsedData.turnOfDay.substr(0, 2)}:${parsedData.turnOfDay.substr(2)}`;

        this.return.push(parsedData);

        if (callback)
            callback();
    }
}

module.exports = IFFCompany;