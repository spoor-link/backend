const IFFFile = require('./IFFFile');

/**
 * Reads and parses countries from the IFF standard.
 */
class IFFCountry extends IFFFile {
    constructor(opts) {
        super(opts);

        this.countryStartDate = null;
        this.countryEndDate = null;
    }

    /**
     * Parse a single line from the file. Called from IFFFile.readFile.
     * @param {String} line - One line from the file
     * @param {Function} callback - Called when it is done reading this line
     */
    parseLine(line, callback) {
        let command = line.substr(0, 1);
        let data = line.substr(1);

        switch (command) {
            case '@': // Start record
                this.parseStart(data, callback);
                break;
            default:
                this.parseCountry(line, callback);
                break;
        }
    }

    /**
     * Parse the start of the file. This contains general information.
     * @param {String} data - Raw data without the command identifier
     * @param {Function=} callback - Called when the start entry is parsed
     */
    parseStart(data, callback) {
        let parsedData = this.splitData(data, [
            'company',
            'startDate',
            'endDate',
            'version',
            'description',
        ]);

        this.countryStartDate = parsedData.startDate;
        this.countryEndDate = parsedData.endDate;

        if (callback)
            callback();
    }

    /**
     * Parse a single country. These are not preceded by a command identifier. The file contains 1 country per line.
     * @param {String} data - The raw country data
     * @param {Function=} callback - Called when the country is parsed
     */
    parseCountry(data, callback) {
        let parsedData = this.splitData(data, [
            'code',
            'inland',
            'fullName',
        ]);

        parsedData.inland = parsedData.inland === 1;

        this.return.push(parsedData);

        if (callback)
            callback();
    }
}

module.exports = IFFCountry;