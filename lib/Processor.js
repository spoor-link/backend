/**
 * Base class for all processors.
 */
class Processor {
    constructor() {
        this.variables = {};
    }

    /**
     * Sets the shared variables so they can be used by Processor.preRun() and Processor.run().
     * This is called when initializing the processor object.
     * @param variables
     */
    setVariables(variables) {
        this.variables = variables;
    }

    /**
     * Get the validation of the processor.
     * If this returns true, Processor.run() will be called at the start of the application.
     * @returns {boolean}
     */
    getValidation() {
        return false;
    }

    /**
     * Get the list of schedules for scheduled tasks.
     * These schedules follow the Cronjob syntax, but have an optional extra field for seconds.
     * At each event occurance, Processor.runSchedule(fireDate) is called.
     * @returns {Array}
     */
    getSchedules() {
        return [];
    }
}

module.exports = Processor;