const moment = require('moment');
const uuid = require('uuid');
const async = require('async');
const ResponseObject = require('./ResponseObject');

class V1WebSocket {
    constructor(wss, variables) {
        this.wss = wss;
        this.variables = variables;

        this.trainQueue = {};

        this.sockets = {};
    }

    /**
     * Initialize the web socket system.
     * This adds listeners for all database events and starts handling for incoming connections
     */
    run() {
        this._assignListeners();

        // Check every 50 milliseconds if trains are ready to be sent to clients by web socket
        // TODO: Extend the callbacks into this._checkUpdatedTrains and restart the sequence 10 ms after the last one has finished, to prevent it from running multiple times
        setInterval(() => {
            this._checkUpdatedTrains();
        }, 50);

        // Send a heartbeat signal every 5 seconds
        setInterval(() => {
            this._sendHeartbeat();
        }, 5000);

        // Handle incoming connections
        this.wss.on('connection', socket => {
            this._handleConnection(socket);
        });
    }

    /**
     * Handle all incoming web socket connections from clients
     * @param {Socket} socket - The socket object from https://github.com/websockets/ws
     * @protected
     */
    _handleConnection(socket) {
        socket.id = uuid();
        this.sockets[socket.id] = {
            socket,
            stations: [],
        };

        this.variables.logging.access.info(`${socket._socket.address().address}: SOCKET OPEN ${socket.id}`);

        // If a client closes the connection
        socket.on('close', (code, reason) => {
            this.variables.logging.access.info(`${socket._socket.address().address}: SOCKET CLOSE ${socket.id}`);
            if (code)
                this.variables.logging.access.info(code);
            if (reason)
                this.variables.logging.access.info(reason);

            delete this.sockets[socket.id];
        });

        // If the client sends a ping
        socket.on('ping', (data) => {
            socket.pong(data);
        });

        // If the client sends a message
        socket.on('message', data => {
            this._handleMessage(socket, data);
        });
    }

    /**
     * Handle a message sent from a web socket client
     * @param {Socket} socket - The socket object from https://github.com/websockets/ws
     * @param {String} data - The data sent from the client
     * @protected
     */
    _handleMessage(socket, data) {
        try {
            data = JSON.parse(data);

            if (typeof data.cmd !== 'undefined') {
                let command = data.cmd;
                data = data.data;

                switch (command) {
                    case 'setStations':
                        this.sockets[socket.id].stations = data;
                        break;
                }
            }
        }
        catch (e) {
            this.variables.logging.general.error(e);
        }
    }

    /**
     * Assigns listeners for database events
     * These events are triggered when a creation, update or deletion on selected tables is done.
     * @protected
     */
    _assignListeners() {
        this.variables.ORM.Train.addHook('afterSave', (train) => {
            this._updateTrain(train.id, 'update');
        });
        this.variables.ORM.RouteEntry.addHook('afterSave', (routeEntry) => {
            this._updateTrain(routeEntry.trainId, 'update');
        });
        this.variables.ORM.Position.addHook('afterSave', (position) => {
            this._updateTrain(position.trainId, 'update');
        });
    }

    /**
     * Queues a train for updating to clients.
     * It adds the train ID to a central queue, with the initial update datetime and last update datetime attached.
     * @param {String} trainId - The train ID
     * @param {String} updateType - Currently unused, the type of update occurred
     * @protected
     */
    _updateTrain(trainId, updateType) {
        if (typeof this.trainQueue[trainId] === 'undefined') {
            this.trainQueue[trainId] = {
                firstUpdate: moment(),
                count: 0,
            };
        }

        this.trainQueue[trainId].lastUpdate = moment();
        this.trainQueue[trainId].count++;
    }

    /**
     * Checks the central train queue for trains that are ready to be sent to clients.
     * Trains are ready when they did not receive an update in the last 400 milliseconds, or it has been 1000 milliseconds since the first update.
     * @protected
     */
    _checkUpdatedTrains() {
        for (let key in this.trainQueue) {
            if (!this.trainQueue.hasOwnProperty(key)) continue;

            let train = this.trainQueue[key];

            // If the last update was more than 400 milliseconds ago, or the first update was more than 1 second ago
            if (moment().diff(train.lastUpdate) >= 400 || moment().diff(train.firstUpdate) >= 1000) {
                delete this.trainQueue[key];

                this._sendTrain(key);
            }
        }
    }

    /**
     * @description determine if an array contains one or more items from another array.
     * @param {Array} haystack - The array to search.
     * @param {Array} arr - The array providing items to check for in the haystack.
     * @return {Boolean} - If haystack contains at least one item from arr.
     * @protected
     */
    _findOne(haystack, arr) {
        return arr.some(function (v) {
            return haystack.indexOf(v) >= 0;
        });
    }

    /**
     * Send a train to clients through connected web sockets.
     * This requires clients to set up the stations they wish to receive updates for (through the setStations command).
     * @param {String} trainId - The UUID of the train which needs to be sent to the clients using their web socket connections
     * @protected
     */
    _sendTrain(trainId) {
        ResponseObject.convertFromTrainId(this.variables.ORM, trainId, (train) => {
            let stations = [];

            // Get a list of the station codes
            async.each(train.routeEntries, (routeEntry, callback) => {
                stations.push(routeEntry.station.code);
                callback();
            }, err => {
                // Loop through all open sockets and send the train if needed
                async.each(this.sockets, (socket, callback) => {

                    // Only send the update if the client has requested updates from at least 1 matching station
                    if (socket.stations.length !== 0) {
                        if (this._findOne(stations, socket.stations)) {
                            try {
                                socket.socket.send(JSON.stringify({
                                    cmd: 'trainUpdate',
                                    data: train,
                                }));
                            }
                            catch (e) {
                                // Pass
                            }
                        }
                    }
                    callback();
                });
            });
        });
    }

    /**
     * Send a heartbeat signal to all connected web sockets.
     * @protected
     */
    _sendHeartbeat() {
        async.each(this.sockets, (socket, callback) => {
            let now = moment();

            socket.socket.send(JSON.stringify({
                cmd: 'heartbeat',
                data: now,
            }));
            callback();
        });
    }
}

module.exports = V1WebSocket;