const schedule = require('node-schedule');
const moment = require('moment');

/**
 * Controls rate limiting for the REST API. By default, it will give an unknown IP address the max hits per day as defined in the .env.
 * But it is also possible to give certain IPs a different limit or allow them to bypass the API limit altogether.
 */
class RateLimit {
    constructor() {
        this.IPs = {};
        this.whitelistedURLs = [
            '/rateLimit',
        ];

        // Reset the limits at 00:00 every day
        schedule.scheduleJob('0 0 0 * * *', () => {
            this._resetLimits();
        })
    }

    /**
     * Check if a given client is able to do requests to the API.
     * @param {String} URL - the path the IP is trying to visit
     * @param {String} IP - the IP of the client
     * @param {Function=} callback - Called when done searching. Contains 1 parameter: true if the client can continue, false if not
     * @constructor
     */
    IPCanRequest(URL, IP, callback) {
        if (typeof this.IPs[IP] === "undefined") {
            if (callback)
                callback(true);
        }
        else if (this.whitelistedURLs.includes(URL)) {
            if (callback)
                callback(true);
        }
        else if (parseInt(this.IPs[IP].remaining) > 0) {
            if (callback)
                callback(true);
        }
        else {
            if (callback)
                callback(false);
        }
    }

    /**
     * Register a hit from a client
     * @param {String} URL - the path the IP is trying to visit
     * @param {String} IP - the IP of the client
     * @param {Function=} callback - Called when the hit processing is done
     */
    registerHit(URL, IP, callback) {
        if (this.whitelistedURLs.includes(URL)) {
            if (callback)
                callback();
        }
        else {
            if (typeof this.IPs[IP] === "undefined") {
                this._addIP(IP, () => {
                    this._addHitToIP(IP, callback);
                });
            }
            else {
                this._addHitToIP(IP, callback);
            }
        }
    }

    /**
     * Get the rate limit information for a given IP. This includes: max hits, remaining hits, if hits are counted and the reset time.
     * @param {String} IP - the IP of the client
     * @param {Function=} callback - Called when lookup is done. Contains 1 parameter: a JSON object of information about the IP
     */
    getIPLimit(IP, callback) {
        if (typeof this.IPs[IP] === "undefined") {
            this._addIP(IP, () => {
                if (callback)
                    callback(this.IPs[IP]);
            });
        }
        else {
            if (callback)
                callback(this.IPs[IP]);
        }
    }

    /**
     * Add a hit to an IP address.
     * @param {String} IP - the IP of the client
     * @param {Function=} callback - Called when the hit processing is done
     * @protected
     */
    _addHitToIP(IP, callback) {
        if (this.IPs[IP].disableLimit !== true)
            this.IPs[IP].remaining = Math.max(0, this.IPs[IP].remaining - 1);

        if (callback)
            callback(this.IPs[IP].remaining);
    }

    /**
     * Add an IP to the list of rate limited IPs to track. It will only att the IP if it's not there already
     * @param {String} IP - the IP of the client
     * @param {Function=} callback - Called when adding the IP is complete
     * @protected
     */
    _addIP(IP, callback) {
        if (typeof this.IPs[IP] === "undefined") {
            let reset = moment().add(1, 'd').startOf('day').toISOString(true); // Set the reset time to the start of the next day
            let limit = parseInt(process.env.rate_limit_default || 10000);

            this.IPs[IP] = {
                max: limit,
                remaining: limit,
                disableLimit: false,
                reset,
            };
        }

        if (callback)
            callback(this.IPs[IP]);
    }

    /**
     * Called once a day to reset the rate limits
     * @protected
     */
    _resetLimits() {
        this.IPs = [];
    }
}

module.exports = RateLimit;