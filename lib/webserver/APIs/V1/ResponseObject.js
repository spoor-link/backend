const async = require('async');
const Sequelize = require('sequelize');

/**
 * Static class which converts objects from the ORM or train IDs to JSON objects ready to serve through the API.
 */
class ResponseObject {
    /**
     * Convert any ORM object into a JSON object with all required nested relations.
     * @param {Object} obj - The object coming from the ORM
     * @param {objectCallback|Null} callback
     */
    static convert(obj, callback) {
        let method = null;

        // Check which type of object it is
        switch (obj.constructor.name) {
            case 'Country':
                method = ResponseObject._convertCountry;
                break;
            case 'Station':
                method = ResponseObject._convertStation;
                break;
            case 'Train':
                method = ResponseObject._convertTrain;
                break;
            case 'RouteEntry':
                method = ResponseObject._convertRouteEntry;
                break;
            case 'TravelerData':
                method = ResponseObject._convertTravelerData;
                break;
        }

        // If a method is found, use it to convert the object
        if (method !== null) {
            method(obj, callback);
        }
        // If no method is found, send the plain object back
        else {
            if (callback)
                callback(obj);
        }
    }

    /**
     * Convert trains into JSON objects using a list of train UUIDs.
     * @param {ORM} ORM - The main ORM class
     * @param {Array} ids - A list of train UUIDs
     * @param {trainsCallback} callback
     */
    static convertFromTrainIds(ORM, ids, callback) {
        ORM.Train.findAll({
            attributes: [
                'id',
                'trainId',
                'trainNumber',
                ['trainDate', 'date'],
                'track',
                'lat',
                'lon',
                'GPSAt',
                ['prevTrainId', 'previousTrainId'],
                'nextTrainId',
            ],
            where: {
                id: {
                    [Sequelize.Op.in]: ids,
                },
            },
            include: [
                {
                    model: ORM.RouteEntry,
                    as: 'routeEntries',
                    attributes: [
                        'id',
                        'trainId',
                        'stopType',
                        'plannedArrival',
                        'currentArrival',
                        'plannedDeparture',
                        'currentDeparture',
                        'plannedTrack',
                        'currentTrack',
                        'announced',
                        'arrived',
                        'departed',
                        'cancelledArrival',
                        'cancelledDeparture',
                    ],
                    include: [
                        {
                            model: ORM.Station,
                            as: 'station',
                            attributes: [
                                'id',
                                'code',
                                'shortName',
                                'middleName',
                                'longName',
                                'type',
                                'lat',
                                'lon',
                            ],
                            include: [
                                {
                                    model: ORM.Country,
                                    as: 'country',
                                    attributes: [
                                        'id',
                                        'code',
                                        'fullName',
                                        'timezone',
                                    ],
                                },
                            ],
                        },
                        {
                            model: ORM.TravelerData,
                            as: 'entryData',
                            attributes: [
                                'id',
                                'reservation',
                                'fee',
                                'noBoarding',
                                'rearTrainPartTerminates',
                                'shunting',
                                'specialTicket',
                                'replacementTransport',
                                'messages',
                            ],
                        },
                    ],
                },
                {
                    model: ORM.Company,
                    as: 'company',
                    attributes: [
                        'id',
                        'companyCode',
                        'code',
                        'fullName',
                    ],
                },
                {
                    model: ORM.TrainType,
                    as: 'trainType',
                    attributes: [
                        'id',
                        'code',
                        'fullName',
                        'passengers',
                    ],
                }, /*
                {
                    model: ORM.Position,
                    as: 'positions',
                    limit: 1,
                    order: [['measuredAt', 'DESC']],
                    attributes: [
                        'id',
                        'trainId',
                        'lat',
                        'lon',
                        'speed',
                        'direction',
                        'measuredAt',
                    ],
                },*/
            ],
            order: Sequelize.literal('IF(plannedArrival IS NOT NULL, plannedArrival, plannedDeparture)'),
        }).then(trains => {
            callback(trains);
        });
    }

    /**
     * Convert a train into a JSON object using it's UUID.
     * @param {ORM} ORM - The main ORM class
     * @param {String} id - The UUID of the train
     * @param {trainCallback} callback
     */
    static convertFromTrainId(ORM, id, callback) {
        ResponseObject.convertFromTrainIds(ORM, [id], (trains) => {
            callback(trains[0]);
        });
    }

    /**
     * Internal method for converting countries.
     * @param {Country} country - Country ORM object instance
     * @param {countryCallback} callback
     * @protected
     */
    static _convertCountry(country, callback) {
        callback({
            id: country.id,
            code: country.code,
            fullName: country.fullName,
            timezone: country.timezone,
        });
    }

    /**
     * Internal method for converting stations.
     * @param {Station} station - Station ORM object instance
     * @param {stationCallback} callback
     * @protected
     */
    static _convertStation(station, callback) {
        let returnStation = {
            id: station.id,
            code: station.code,
            shortName: station.shortName,
            middleName: station.middleName,
            longName: station.longName,
            type: station.type,
            lat: station.lat,
            lon: station.lon,
        };

        // Get the country
        station.getCountry().then(country => {
            ResponseObject.convert(country, returnCountry => {
                returnStation.country = returnCountry;

                callback(returnStation);
            });
        })
    }

    /**
     * Internal method for converting traveler data.
     * @param {TravelerData} entryData - Traveler data ORM object instance
     * @param {travelerDataCallback} callback
     * @protected
     */
    static _convertTravelerData(entryData, callback) {
        callback({
            id: entryData.id,
            reservation: entryData.reservation,
            fee: entryData.fee,
            noBoarding: entryData.noBoarding,
            rearTrainPartTerminates: entryData.rearTrainPartTerminates,
            shunting: entryData.shunting,
            specialTicket: entryData.specialTicket,
            replacementTransport: entryData.replacementTransport,
            messages: entryData.messages,
        });
    }

    /**
     * Internal method for converting route entries.
     * @param {RouteEntry} routeEntry - Route entry ORM object instance
     * @param {routeEntryCallback} callback
     * @protected
     */
    static _convertRouteEntry(routeEntry, callback) {
        let returnEntry = {
            id: routeEntry.id,
            trainId: routeEntry.trainId,
            stopType: routeEntry.stopType,
            plannedArrival: routeEntry.plannedArrival,
            currentArrival: routeEntry.currentArrival,
            plannedDeparture: routeEntry.plannedDeparture,
            currentDeparture: routeEntry.currentDeparture,
            plannedTrack: routeEntry.plannedTrack,
            currentTrack: routeEntry.currentTrack,
            arrived: routeEntry.arrived,
            departed: routeEntry.departed,
            cancelledArrival: routeEntry.cancelledArrival,
            cancelledDeparture: routeEntry.cancelledDeparture,
        };

        // Get the station
        routeEntry.getStation().then(station => {
            ResponseObject.convert(station, returnStation => {
                returnEntry.station = returnStation;

                routeEntry.getEntryData().then(entryData => {
                    ResponseObject.convert(entryData, returnEntryData => {
                        returnEntry.entryData = returnEntryData;

                        callback(returnEntry);
                    })
                })
            });
        });
    }

    /**
     * Internal method for converting trains.
     * This method is slow, we recommend using ResponseObject.convertFromTrainIds or ResponseObject.convertFromTrainId instead.
     * @param {Train} train - Train ORM object instance
     * @param {trainCallback} callback
     * @protected
     */
    static _convertTrain(train, callback) {
        let returnTrain = {
            id: train.id,
            trainId: train.trainId,
            trainNumber: train.trainNumber,
            date: train.trainDate,
            track: train.track,
            previousTrainId: train.previousTrainId,
            nextTrainId: train.nextTrainId,
            routeEntries: [],
        };

        train.getRouteEntries({
            order: Sequelize.literal('IF(plannedArrival IS NOT NULL, plannedArrival, plannedDeparture) ASC'),
        }).then(routeEntries => {
            async.eachSeries(routeEntries, (entry, callback) => {
                ResponseObject.convert(entry, (returnEntry) => {
                    returnTrain.routeEntries.push(returnEntry);
                    callback();
                });
            }, err => {
                callback(returnTrain);
            });
        });

    }
}

/**
 * Callback used for returning a JSON object.
 * @callback objectCallback
 * @param {Object} returnObject - A converted JSON object with nested information
 */

/**
 * Callback used for returning a JSON object for trains.
 * @callback trainsCallback
 * @param {Array} returnTrains - A JSON object with the trains
 */

/**
 * Callback used for returning a JSON object for a train.
 * @callback trainCallback
 * @param {Object} returnTrain - A JSON object with the train
 */

/**
 * Callback used for returning a JSON object for a country.
 * @callback countryCallback
 * @param {Object} returnCountry - A JSON object with the country
 */

/**
 * Callback used for returning a JSON object for a station.
 * @callback stationCallback
 * @param {Object} returnStation - A JSON object with the station
 */

/**
 * Callback used for returning a JSON object for traveler data.
 * @callback travelerDataCallback
 * @param {Object} returnTravelerData - A JSON object with the traveler data
 */

/**
 * Callback used for returning a JSON object for a route entry.
 * @callback routeEntryCallback
 * @param {Object} returnEntry - A JSON object with the route entry
 */

/**
 * Callback used for returning a JSON object for a train.
 * @callback trainCallback
 * @param {Object} returnTrain - A JSON object with the train
 */


module.exports = ResponseObject;