const V1Routes = require('./routes');
const V1WebSocket = require('./V1WebSocket');

/**
 * Bootstraps the V1 API.
 */
class V1API {
    /**
     *
     * @param {Router} router - The ExpressJS router object
     * @param {WebSocket} wss - The web socket object
     * @param {Object} variables - A JSON object of all variables available in the system
     */
    constructor(router, wss, variables) {
        this.router = router;

        this.routes = new V1Routes(this.router, variables);
        this.routes.addRoutes();

        this.websocket = new V1WebSocket(wss, variables);
        this.websocket.run();
    }
}

module.exports = V1API;