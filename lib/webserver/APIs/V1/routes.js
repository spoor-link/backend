const swaggerUi = require('swagger-ui-express');
const fs = require('fs');
const path = require('path');
const async = require('async');
const moment = require('moment');
const dijkstra = require('../../../dijkstra');
const RateLimit = require('./RateLimit');

const swaggerDocument = require('./assets/API.json');

const ResponseObject = require('./ResponseObject');
const View = require('./View');

/**
 * Set all routes used by the V1 API.
 */
class V1Routes {
    constructor(router, variables) {
        this.router = router;
        this.variables = variables;

        this.Op = this.variables.ORM.Sequelize.Op;

        this.rateLimiter = null;
    }

    /**
     * Add all routes
     */
    addRoutes() {
        this._rateLimiting();
        this._root();
        this._stations();
        this._stationsRoute();
        this._stationById();
        this._countries();
        this._trains();
        this._trainById();
        this._trainByDateAndNumber();
        this._interruptions();
        this._interruptionsByStation();
    }

    /**
     * Send a code 500 response to the client, in case of errors.
     * @param {Request} req - The ExpressJS request object
     * @param {Response} res - The ExpressJS response object
     * @protected
     */
    _send500(req, res) {
        View.res500(req, res, [
            {'en_GB': 'Internal server error'},
        ]);
    }

    /**
     * Set the middleware and route for the rate limiting.
     * @protected
     */
    _rateLimiting() {
        this.rateLimiter = new RateLimit();

        this.router.use((req, res, next) => {
            this.rateLimiter.IPCanRequest(req.path, req.ip, (canRequest) => {
                if (canRequest === true) {
                    this.rateLimiter.registerHit(req.path, req.ip);
                    next();
                }
                else {
                    // Send an error response
                    res.status(403);
                    return res.send({
                        code: 'RATE-OVER',
                        messages: [
                            {
                                language: 'en_GB',
                                message: 'You have reached the API limit.',
                            }
                        ],
                    });
                }
            });
        });

        this.router.get('/rateLimit', (req, res) => {
            try {
                this.rateLimiter.getIPLimit(req.ip, (IPLimit) => {
                    View.res200(req, res, IPLimit);
                });
            }
            catch (e) {
                this.variables.logging.general.error(e);
                this._send500(req, res);
            }
        });
    }

    /**
     * Set the routes and cross origin stuff for the root of the API (/v1).
     * @protected
     */
    _root() {
        // Allow cross origin requests
        this.router.use((req, res, next) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            next();
        });

        // Set the root page to the API documentation
        this.router.use('/', swaggerUi.serve);
        this.router.get('/', swaggerUi.setup(swaggerDocument, {
            customSiteTitle: 'spoor.link API V1',
            customfavIcon: fs.readFileSync(path.join(process.cwd(), '/lib/webserver/APIs/V1/assets/ico.bin')),
        }));
    }

    /**
     * Set the route for /stations.
     * @protected
     */
    _stations() {
        this.router.get('/stations', (req, res) => {
            try {
                this.variables.ORM.Station.findAll().then(stations => {
                    View.res200(req, res, stations);
                });
            }
            catch (e) {
                this.variables.logging.general.error(e);
                this._send500(req, res);
            }
        });
    }

    /**
     * Set the route for /stations/route.
     * @protected
     */
    _stationsRoute() {
        this.router.get('/stations/route', (req, res) => {
            try {
                let stations = req.query.stations;

                // Split the station list into an array of trimmed upper case station codes
                if (typeof stations !== 'undefined') {
                    stations = stations.split(',');

                    for (let i = 0; i < stations.length; i++) {
                        stations[i] = stations[i].toString().toUpperCase().trim();
                    }
                }

                let filePath = path.join(process.cwd(), process.env.timetable_statconn); // File with station connections
                dijkstra.graph.map = JSON.parse(fs.readFileSync(filePath, 'utf8'));
                stations = dijkstra.graph.findShortestPath(stations); // Find the shortest path, returns a list of station codes

                // Find all station objects belonging to the codes
                this.variables.ORM.Station.findAll({
                    where: {
                        code: {
                            [this.Op.in]: stations,
                        },
                    },
                }).then(DBStations => {
                    let stationList = {};

                    // Create a new list with all station objects, in the correct order
                    async.eachSeries(DBStations, (station, callback) => {
                        stationList[station.code] = station;
                        callback();
                    }, err => {
                        for (let i = 0; i < stations.length; i++) {
                            stations[i] = stationList[stations[i]];
                        }

                        View.res200(req, res, stations);
                    });
                });
            }
            catch (e) {
                this.variables.logging.general.error(e);
                this._send500(req, res);
            }
        });
    }

    /**
     * Set the route for /station/{id}.
     * @protected
     */
    _stationById() {
        this.router.get('/stations/:id', (req, res) => {
            try {
                this.variables.ORM.Station.findOne({
                    where: {
                        [this.Op.or]: [
                            {id: req.params.id,},
                            {code: req.params.id.toUpperCase()},
                            {shortName: `%${req.params.id}%`}, // These aren't working atm
                            {middleName: `%${req.params.id}%`},
                            {longName: `%${req.params.id}%`},
                        ],
                    }
                }).then(station => {
                    if (station)
                        View.res200(req, res, station);
                    else {
                        View.res404(req, res, {
                            'en_GB': 'Station not found',
                        });
                    }
                });
            }
            catch (e) {
                this.variables.logging.general.error(e);
                this._send500(req, res);
            }
        });
    }

    /**
     * Set the route for /countries.
     * @protected
     */
    _countries() {
        this.router.get('/countries', (req, res) => {
            try {
                this.variables.ORM.Country.findAll().then(countries => {
                    View.res200(req, res, countries);
                });
            }
            catch (e) {
                this.variables.logging.general.error(e);
                this._send500(req, res);
            }
        });
    }

    /**
     * Set the route for /trains.
     * @protected
     */
    _trains() {
        this.router.get('/trains', (req, res) => {
            try {
                let stations = req.query.stations;
                let startAt = req.query.startAt;
                let endAt = req.query.endAt;

                if (typeof stations !== 'undefined') {
                    stations = stations.split(',');

                    for (let i = 0; i < stations.length; i++) {
                        stations[i] = stations[i].toString().toUpperCase().trim();
                    }
                }

                if (typeof startAt !== 'undefined')
                    startAt = moment(startAt);

                if (typeof endAt !== 'undefined')
                    endAt = moment(endAt);
                else
                    endAt = moment();

                // Stations, startAt and endAt are required
                if (typeof stations !== 'undefined' && startAt.isValid() && endAt.isValid()) {
                    this.variables.ORM.Station.findAll({
                        where: {
                            code: {
                                [this.Op.in]: stations,
                            },
                        },
                    }).then(stations => {
                        if (stations.length > 0) {
                            let stationIds = [];

                            for (let i = 0; i < stations.length; i++) {
                                stationIds.push(stations[i].id);
                            }

                            this.variables.ORM.RouteEntry.findAll({
                                attributes: ['trainId'],
                                where: {
                                    stationId: {
                                        [this.Op.in]: stationIds,
                                    },
                                    [this.Op.or]: [
                                        {
                                            plannedArrival: {
                                                [this.Op.between]: [startAt.format('YYYY-MM-DD HH:mm:ss'), endAt.format('YYYY-MM-DD HH:mm:ss')],
                                            },
                                        },
                                        {
                                            plannedDeparture: {
                                                [this.Op.between]: [startAt.format('YYYY-MM-DD HH:mm:ss'), endAt.format('YYYY-MM-DD HH:mm:ss')],
                                            },
                                        },
                                    ],
                                },
                            }).then(trainIds => {

                                for (let i = 0; i < trainIds.length; i++) {
                                    trainIds[i] = trainIds[i].trainId;
                                }

                                View.res200Trains(req, res, this.variables.ORM, trainIds);
                            });
                        }
                        else {
                            // No stations found, so the train list will be empty
                            View.res200(req, res, []);
                        }
                    });
                }
                else {
                    View.res400(req, res, {
                        'en_GB': 'Illegal arguments',
                    });
                }
            }
            catch (e) {
                this.variables.logging.general.error(e);
                this._send500(req, res);
            }
        });
    }

    /**
     * Set the route for /trains/{id}.
     * @protected
     */
    _trainById() {
        this.router.get('/trains/:id', (req, res) => {
            try {
                this.variables.ORM.Train.findOne({
                    where: {
                        id: req.params.id,
                    }
                }).then(train => {
                    if (train)
                        View.res200(req, res, train);
                    else {
                        View.res404(req, res, {
                            'en_GB': 'Train not found',
                        });
                    }
                });
            }
            catch (e) {
                this.variables.logging.general.error(e);
                this._send500(req, res);
            }
        });
    }

    /**
     * Set the route for /trains/{date}/{trainNumber}.
     * @protected
     */
    _trainByDateAndNumber() {
        this.router.get('/trains/:date/:trainNumber', (req, res) => {
            try {
                let trainDate = moment(req.params.date, 'YYYY-MM-DD');
                if (!trainDate.isValid())
                    trainDate = moment(req.params.date);

                let trainNumber = req.params.trainNumber;

                if (typeof req.params.date !== 'undefined' && typeof  req.params.trainNumber !== 'undefined' && trainDate.isValid()) {
                    this.variables.ORM.Train.findOne({
                        where: {
                            trainDate: trainDate.format('YYYY-MM-DD'),
                            [this.Op.or]: [
                                {trainId: trainNumber,},
                                {trainNumber},
                            ],
                        }
                    }).then(train => {
                        if (train)
                            View.res200(req, res, train);
                        else {
                            View.res404(req, res, {
                                'en_GB': 'Train not found',
                            });
                        }
                    });
                }
                else {
                    View.res400(req, res, {
                        'en_GB': 'Illegal arguments',
                    });
                }
            }
            catch (e) {
                this.variables.logging.general.error(e);
                this._send500(res);
            }
        });
    }

    /**
     * Dummy method for upcoming interruptions
     * @protected
     */
    _interruptions() {
        this.router.get('/interruptions', (req, res) => {
            try {
                View.res200(req, res, []);
            }
            catch (e) {
                this.variables.logging.general.error(e);
                this._send500(res);
            }
        });
    }

    /**
     * Dummy method for upcoming interruptions
     * @protected
     */
    _interruptionsByStation() {
        this.router.get('/interruptions/{station}', (req, res) => {
            try {
                View.res200(req, res, []);
            }
            catch (e) {
                this.variables.logging.general.error(e);
                this._send500(res);
            }
        });
    }
}

module.exports = V1Routes;