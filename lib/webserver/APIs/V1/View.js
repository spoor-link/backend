const ResponseObject = require('./ResponseObject');
const async = require('async');

/**
 * All routes from the V1 API use this class to send responses to the client.
 */
class View {
    /**
     * Check if the input is an array.
     * @param {object} input - The input to test
     * @return {boolean} - True if the input is an array
     * @protected
     */
    static _isArray(input) {
        return (!!input) && (input.constructor === Array);
    }

    /**
     * Parse error messages to the client by adding the error code to it.
     * @param {Number} code - The HTTP status code
     * @param {Array} messages - The messages to be displayed to the client
     * @return {{code: *, messages: *}}
     * @protected
     */
    static _parseMessages(code, messages) {
        if (!View._isArray(messages))
            messages = [messages];

        return {
            code,
            messages,
        };
    }

    /**
     * Handles a response to the client. This prepares the data for pagination and converts it to JSON
     * @param {Request} req - The web server Request object
     * @param {Response} res - The web server Response object
     * @param {Number} code - The HTTP status code to be used for the response
     * @param {object} data - The data that should be sent to the client
     */
    static res(req, res, code, data) {
        res.status(code);
        try {
            if (View._isArray(data)) {
                let responseList = [];

                async.eachSeries(data, (obj, callback) => {
                    ResponseObject.convert(obj, returnObj => {
                        responseList.push(returnObj);
                        callback();
                    });
                }, err => {
                    if (err) {
                        console.log(err);
                        View.res500(req, res, {
                            'en_GB': 'Internal server error',
                        });
                    }
                    else {
                        View._paginatedRes(req, res, responseList);
                    }
                });
            }
            else if (typeof data === 'object') {
                ResponseObject.convert(data, returnObj => {
                    View._paginatedRes(req, res, returnObj);
                });
            }
            else {
                View._paginatedRes(req, res, data);
            }
        }
        catch (e) {
            console.error(e);
            View.res500(req, res, 'An error occurred');
        }
    }

    /**
     * Determines if the returned data should be paginated or not, sets up the pagination and sends the data to the client.
     * Data is considered paginated of "data" contains an array, even if it only contains 1 item.
     * @param {Request} req - The web server Request object
     * @param {Response} res - The web server Response object
     * @param {object} data - The data that should be sent to the client
     * @param {Number?} totalObjects - Optional, the total amount of objects
     * @protected
     */
    static _paginatedRes(req, res, data, totalObjects) {
        if (View._isArray(data)) {
            if (typeof totalObjects !== 'number')
                totalObjects = data.length;

            let limit = totalObjects;
            let page = 1;

            // Create the URLs
            let currentUrl = `${req.protocol}://${req.hostname}:${process.env.api_port || 3000}${req.originalUrl}`;

            if (currentUrl.endsWith(req.path))
                currentUrl = `${currentUrl}?`;
            else {
                currentUrl = `${currentUrl}&`;

                // Check if limit and/or page parameters are set
                if (typeof req.query.limit !== 'undefined') {
                    currentUrl = currentUrl.replace(`limit=${req.query.limit}`, '');
                    limit = parseInt(req.query.limit);
                }
                if (typeof req.query.page !== 'undefined') {
                    currentUrl = currentUrl.replace(`page=${req.query.page}`, '');
                    page = parseInt(req.query.page);
                }
            }

            // Crop the data to the limit/page, but only if that wasn't done by the route
            if (totalObjects === data.length) {
                let start = (page - 1) * limit;
                let end = start + limit;
                data = data.slice(start, end);
            }

            let lastPage = Math.ceil(totalObjects / limit);

            let firstPageUrl = `${currentUrl}limit=${limit}&page=1`;
            let lastPageUrl = `${currentUrl}limit=${limit}&page=${lastPage}`;
            let nextPageUrl = '';
            if (lastPage > page)
                nextPageUrl = `${currentUrl}limit=${limit}&page=${page + 1}`;

            let prevPageUrl = '';
            if (page > 1)
                prevPageUrl = `${currentUrl}limit=${limit}&page=${page - 1}`;

            res.send({
                data,
                paging: {
                    totalObjects,
                    objectsPerPage: data.length,
                    fromObject: (limit * (page - 1)) + 1,
                    toObject: limit * (page),
                    currentPage: page,
                    totalPages: lastPage,
                    firstPageUrl,
                    lastPageUrl,
                    nextPageUrl,
                    prevPageUrl,
                }
            });
        }
        else {
            res.send(data);
        }
    }

    /**
     * Shortcut method to send a response with HTTP code 200
     * @param {Request} req - The web server Request object
     * @param {Response} res - The web server Response object
     * @param {object} data - The data that should be sent to the client
     */
    static res200(req, res, data) {
        View.res(req, res, 200, data);
    }

    /**
     * Shortcut method to send a response with HTTP code 200 from train IDs
     * @param {Request} req - The web server Request object
     * @param {Response} res - The web server Response object
     * @param {ORM} ORM - The database object, because it needs to look up the trains
     * @param {Array} data - The data that should be sent to the client
     */
    static res200Trains(req, res, ORM, data) {
        res.status(200);
        ResponseObject.convertFromTrainIds(ORM, data, trains => {
            View._paginatedRes(req, res, trains);
        })
    }

    /**
     * Shortcut method to send a response with HTTP code 400
     * @param {Request} req - The web server Request object
     * @param {Response} res - The web server Response object
     * @param {Array} messages - The messages for the client, can be in multiple languages
     */
    static res400(req, res, messages) {
        res.status(400);
        res.send(View._parseMessages(400, messages));
    }

    /**
     * Shortcut method to send a response with HTTP code 404
     * @param {Request} req - The web server Request object
     * @param {Response} res - The web server Response object
     * @param {Array} messages - The messages for the client, can be in multiple languages
     */
    static res404(req, res, messages) {
        res.status(404);
        res.send(View._parseMessages(404, messages));
    }

    /**
     * Shortcut method to send a response with HTTP code 500
     * @param {Request} req - The web server Request object
     * @param {Response} res - The web server Response object
     * @param {Array} messages - The messages for the client, can be in multiple languages
     */
    static res500(req, res, messages) {
        res.status(500);
        res.send(View._parseMessages(500, messages));
    }
}

module.exports = View;