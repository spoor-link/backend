const async = require('async');
const moment = require('moment');
const Processor = require('../Processor');

/**
 * This class cleans arrivals and departures from route entries.
 * If they are 10 or more minutes past due and are not fulfilled yet, they are set to their appropriate state.
 */
class CleanRouteEntriesProcessor extends Processor {
    constructor() {
        super();

        this.cleanupRunning = false;

        this.cleanupThreshold = 15; // In minutes
    }

    getValidation() {
        return !!this.variables.ORM.RouteEntry;
    }

    getSchedules() {
        return [
            '2 * * * * *',
        ];
    }

    preRun(callback) {
        let threshold = moment().subtract(this.cleanupThreshold, 'minutes');
        threshold = threshold.seconds(0);

        // We run the departure checking first, because it sets it's route entries to arrived as well, reducing the workload for checking arrivals
        this._checkDepartures(threshold, () => {
            this._checkArrivals(threshold, () => {
                callback();
            });
        });
    }

    runSchedule(fireDate) {
        let threshold = fireDate.subtract(this.cleanupThreshold, 'minutes');
        threshold = threshold.seconds(0);

        if (!this.cleanupRunning) { // To prevent another cleanup job starts while the old one is still running
            this.cleanupRunning = true;
            this._checkDepartures(threshold, () => {
                this._checkArrivals(threshold, () => {
                    this.cleanupRunning = false;
                });
            });
        }
    }

    /**
     * Check for arrivals to be cleaned up.
     * @param {moment.Moment} threshold - The cutoff for setting trains to arrived. Usually 10 minutes in the past
     * @param {Function} callback - Called when the arrival checking is complete
     * @protected
     */
    _checkArrivals(threshold, callback) {
        let arrivedTrains = 0;

        this.variables.ORM.RouteEntry.findAll({
            where: {
                // If the arrival was more than 10 minutes ago, set it to arrived
                currentArrival: {
                    [this.variables.ORM.Sequelize.Op.not]: null,
                    [this.variables.ORM.Sequelize.Op.lte]: threshold.format('YYYY-MM-DD HH:mm:ss'),
                },
                arrived: {
                    [this.variables.ORM.Sequelize.Op.not]: true,
                },
            },
        }).then(routeEntries => {
            async.eachSeries(routeEntries, (routeEntry, callback) => {
                if ((arrivedTrains % 100) === 0 && arrivedTrains !== 0)
                    this.variables.logging.general.info(`Set ${arrivedTrains} route entries to arrived`);

                arrivedTrains++;

                routeEntry.arrived = true;
                routeEntry.save().then(() => {
                    callback();
                });
            }, err => {
                if (arrivedTrains !== 0)
                    this.variables.logging.general.info(`Set total of ${arrivedTrains} route entries to arrived`);

                if (callback)
                    callback();
            });
        });
    }

    /**
     * Check for departures to be cleaned up.
     * @param {moment.Moment} threshold - The cutoff for setting trains to departed. Usually 10 minutes in the past
     * @param {Function} callback - Called when the departure checking is complete
     * @protected
     */
    _checkDepartures(threshold, callback) {
        let departedTrains = 0;

        this.variables.ORM.RouteEntry.findAll({
            where: {
                [this.variables.ORM.Sequelize.Op.or]: [
                    {
                        // If the departure was more than 10 minutes ago, set it to departed
                        currentDeparture: {
                            [this.variables.ORM.Sequelize.Op.not]: null,
                            [this.variables.ORM.Sequelize.Op.lte]: threshold.format('YYYY-MM-DD HH:mm:ss'),
                        },
                    },
                    {
                        // If the train does not depart (terminal station), set the train to departed 10 minutes after arrival
                        currentDeparture: null,
                        currentArrival: {
                            [this.variables.ORM.Sequelize.Op.not]: null,
                            [this.variables.ORM.Sequelize.Op.lte]: threshold.format('YYYY-MM-DD HH:mm:ss'),
                        },
                    },
                ],
                departed: {
                    [this.variables.ORM.Sequelize.Op.not]: true,
                },
            },
        }).then(routeEntries => {
            async.eachSeries(routeEntries, (routeEntry, callback) => {
                if ((departedTrains % 100) === 0 && departedTrains !== 0)
                    this.variables.logging.general.info(`Set ${departedTrains} route entries to departed`);

                departedTrains++;

                routeEntry.arrived = true;
                routeEntry.departed = true;
                routeEntry.save().then(() => {
                    callback();
                });
            }, err => {
                if (departedTrains !== 0)
                    this.variables.logging.general.info(`Set total of ${departedTrains} route entries to departed`);

                if (callback)
                    callback();
            });
        });
    }
}

module.exports = CleanRouteEntriesProcessor;