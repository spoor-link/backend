const async = require('async');
const moment = require('moment');
const Processor = require('../Processor');
const ArchiveDay = require('./ArchiveTimetableProcessor/ArchiveDay');

/**
 * Archive trains from previous days into SQLite databases.
 * This is to prevent the main database from becoming too large and too slow.
 * These databases are currently not accessible by the API or other components.
 */
class ArchiveTimetableProcessor extends Processor {
    constructor() {
        super();
    }

    getValidation() {
        return true;
    }

    getSchedules() {
        return [
            '6 0 3 * * *',
        ];
    }

    runSchedule(fireDate) {
        this._archiveTrains();
    }

    /**
     * Archive trains from all previous days.
     * @param {Function=} callback
     * @protected
     */
    _archiveTrains(callback) {
        let cutoffDate = moment().subtract(process.env.archive_keep_days || 2, 'days');

        // Find all days for which trains exist
        this.variables.ORM.Train.findAll({
            attributes: [[this.variables.ORM.Sequelize.literal('DISTINCT `trainDate`'), 'trainDate']],
        }).then(trainDates => {

            // Check each date to see if it required archiving
            async.eachSeries(trainDates, (date, dateCallback) => {
                date = moment(date.trainDate);

                // If the date is at or before the cutoff, archive it
                if (date.isSameOrBefore(cutoffDate, 'day')) {
                    this._archiveDay(date, dateCallback);
                }
                else {
                    dateCallback();
                }
            }, err => {
                if (err)
                    this.variables.logging.general.error(err);

                if (callback)
                    callback();
            });
        });
    }

    /**
     * Archive a single day.
     * @param {moment.Moment} date - The date for which trains should be archived
     * @param {Function} callback - Called when the day has completed archiving
     * @protected
     */
    _archiveDay(date, callback) {
        this.variables.logging.general.info(`Archiving all trains for ${date.format('YYYY-MM-DD')}`);

        let archiveDay = new ArchiveDay(date, this.variables);
        archiveDay.run(callback);
    }
}

module.exports = ArchiveTimetableProcessor;