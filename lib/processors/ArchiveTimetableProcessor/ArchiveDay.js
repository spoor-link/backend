const SQLiteFile = require('./SQLiteFile');
const fs = require('fs');
const path = require('path');
const async = require('async');

/**
 * Controls the archiving of a single day from the timetable into an SQLite database.
 */
class ArchiveDay {
    constructor(date, variables) {
        this.date = date;
        this.variables = variables;

        this.sqlite = null;

        this.data = {
            trains: [],
            routeEntries: [],
            travelerData: [],
            positions: [],
        };

        this.eachLimit = 100;
    }

    /**
     * Main class flow.
     * @param {Function=} callback - Called after archiving is complete
     */
    run(callback) {
        async.series([
            callback => {
                this._createDatabase(callback);
            },
            callback => {
                this._getTrains(callback);
            },
            callback => {
                this._archiveData(callback);
            },
            callback => {
                this._cleanup(callback);
            },
        ], err => {
            if (callback)
                callback();
        });
    }

    /**
     * Create the SQLite database and add the tables to it.
     * @param {Function} callback - Called after the database file is created
     * @protected
     */
    _createDatabase(callback) {
        this.sqlite = new SQLiteFile();

        let archiveFolder = path.join(process.cwd(), 'data', 'archive');
        let filePath = path.join(archiveFolder, `${this.date.format('YYYY-MM-DD')}.sqlite`);

        if (!fs.existsSync(archiveFolder))
            fs.mkdirSync(archiveFolder);

        this.sqlite.createSkeleton(filePath, callback);
    }

    /**
     * Get all trains and add them to the archive.
     * @param {Function} callback - Called after all trains from the specified day are archived
     * @protected
     */
    _getTrains(callback) {
        let trainCount = 0;

        this.variables.ORM.Train.findAll({
            where: {
                trainDate: this.date.format('YYYY-MM-DD'),
            },
        }).then(trains => {
            async.eachLimit(trains, this.eachLimit, (train, callback) => {
                if ((trainCount % 100) === 0 && trainCount !== 0)
                    this.variables.logging.general.info(`Archived ${trainCount} trains`);

                trainCount++;

                this._archiveTrain(train, callback);
            }, err => {
                if (err)
                    this.variables.logging.general.error(err);

                this.variables.logging.general.info(`Archived a total of ${trainCount} trains`);

                if (callback)
                    callback();
            });
        });
    }

    /**
     * Archive a single train. This adds the train data to the list of trains which will be archived, and deletes the train.
     * @param {Model} train - The train which will be archived
     * @param {Function=} callback - Called when the entire train is added to the data and deleted
     * @protected
     */
    _archiveTrain(train, callback) {
        // Archive route entries
        train.getRouteEntries().then(routeEntries => {
            async.eachLimit(routeEntries, this.eachLimit, (routeEntry, callback) => {
                this._archiveRouteEntry(routeEntry, callback);
            }, err => {
                if (err)
                    this.variables.logging.general.error(err);

                // Archive positions
                train.getPositions().then(positions => {
                    async.eachLimit(positions, this.eachLimit, (position, callback) => {
                        this._archivePosition(position, callback);
                    }, err => {
                        if (err)
                            this.variables.logging.general.error(err);

                        // Archive the train itself
                        this.data.trains.push(train.toJSON());
                        train.destroy({force: true});
                        if (callback)
                            callback();
                    });
                });
            });
        });
    }

    /**
     * Archive a single route entry. This adds the route entry to the list of route entries which will be archived and deletes the route entry.
     * @param {Model} routeEntry - The route entry that will be archived
     * @param {Function=} callback - Called after the route entry is added to the data and is deleted
     * @protected
     */
    _archiveRouteEntry(routeEntry, callback) {
        routeEntry.getEntryData().then(entryData => {
            this._archiveTravelerData(entryData, () => {
                // Archive the route entry
                this.data.routeEntries.push(routeEntry.toJSON());
                routeEntry.destroy({force: true});
                if (callback)
                    callback();
            })
        });
    }

    /**
     * Archive a single traveler data entry. This adds the traveler data to the list of traveler data information which will be archived and deletes the traveler data.
     * @param {Model} travelerData - The traveler data that will be archived
     * @param {Function=} callback - Called after the traveler data is added to the data and is deleted
     * @protected
     */
    _archiveTravelerData(travelerData, callback) {
        this.data.travelerData.push(travelerData.toJSON());
        travelerData.destroy({force: true});
        if (callback)
            callback();
    }

    /**
     * Archive a single position. This adds the position to the list of route entries which will be archived and deletes the position.
     * @param {Model} position - The position that will be archived
     * @param {Function=} callback - Called after the position is added to the data and is deleted
     * @protected
     */
    _archivePosition(position, callback) {
        this.data.positions.push(position.toJSON());
        position.destroy({force: true});
        if (callback)
            callback();
    }

    /**
     * Send all collected information to the SQLite database.
     * @param {Function=} callback - Called after all data is added to the archive
     * @protected
     */
    _archiveData(callback) {
        this.variables.logging.general.info('Archiving trains');
        this.sqlite.ORM.Train.bulkCreate(this.data.trains).then(() => {

            this.variables.logging.general.info('Archiving route entries');
            this.sqlite.ORM.RouteEntry.bulkCreate(this.data.routeEntries).then(() => {

                this.variables.logging.general.info('Archiving traveler data');
                this.sqlite.ORM.TravelerData.bulkCreate(this.data.travelerData).then(() => {

                    this.variables.logging.general.info('Archiving positions');
                    this.sqlite.ORM.Position.bulkCreate(this.data.positions).then(() => {
                        if (callback)
                            callback();
                    });
                });
            });
        });
    }

    /**
     * Clean up all objects and instances, so the lock to the SQLite database will be released.
     * @param {Function=} callback - Called after the cleanup is complete
     * @protected
     */
    _cleanup(callback) {
        // This will release the lock after a couple of minutes
        this.sqlite.sequelize.close().then(() => {
            this.sqlite.sequelize = null;
            this.sqlite = null;

            if (callback)
                callback();
        });
    }
}

module.exports = ArchiveDay;