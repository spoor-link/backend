const Sequelize = require('sequelize');
const async = require('async');

/**
 * Handles an SQLite file. It is created to manage the SQLite part of archiving old data, but it should be able to read SQLite files as well.
 * If the file already exists, it adds any missing tables.
 */
class SQLiteFile {
    constructor() {
        this.sequelize = null;
        this.ORM = {};
    }

    /**
     * Create the database skeleton. This consists of the database file itself and the tables within.
     * @param {String} file - the path to the SQLite file
     * @param {Function=} callback - Called when the database file is created and populated with tables
     */
    createSkeleton(file, callback) {
        this.sequelize = new Sequelize('null', 'null', 'null', {
            dialect: 'sqlite',
            operatorsAliases: false,

            storage: file,
            logging: false,
        });

        async.series([
            callback => {
                this._createTrainsTable(callback);
            },
            callback => {
                this._createRouteEntriesTable(callback);
            },
            callback => {
                this._createTravelerDataTable(callback);
            },
            callback => {
                this._createPositionsTable(callback);
            },
        ], err => {
            if (callback)
                callback();
        });
    }

    /**
     * Create the trains table.
     * @param {Function=} callback - Called when the trains table is created
     * @protected
     */
    _createTrainsTable(callback) {
        this.ORM.Train = this.sequelize.define('Train', {
            id: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true,
                allowNull: false,
            },
            trainId: {
                type: Sequelize.STRING(45),
                allowNull: false,
            },
            trainNumber: {
                type: Sequelize.STRING(45),
                allowNull: false,
            },
            trainDate: {
                type: Sequelize.DATEONLY,
                allowNull: false,
            },
            status: {
                type: Sequelize.INTEGER,
                allowNull: false,
                defaultValue: 0,
            },
            track: {
                type: Sequelize.STRING(45),
                allowNull: true,
            },
            comments: {
                type: Sequelize.TEXT,
                allowNull: true,
            },
            plannedStartId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
            currentStartId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
            plannedDestinationId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
            currentDestinationId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
            companyId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
            trainTypeId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
            lat: {
                type: Sequelize.DOUBLE(10, 7),
                allowNull: false,
            },
            lon: {
                type: Sequelize.DOUBLE(10, 7),
                allowNull: false,
            },
            prevTrainId: {
                type: Sequelize.UUID,
                allowNull: true,
            },
            nextTrainId: {
                type: Sequelize.UUID,
                allowNull: true,
            },
        });
        this.ORM.Train.sync().then(() => {
            if (callback)
                callback();
        });
    }

    /**
     * Create the route entries table.
     * @param {Function=} callback - Called when the route entries table is created
     * @protected
     */
    _createRouteEntriesTable(callback) {
        this.ORM.RouteEntry = this.sequelize.define('RouteEntry', {
            id: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true,
                allowNull: false,
            },
            stopType: {
                type: Sequelize.ENUM('f', 's', 'p'),
                allowNull: false,
            },
            announced: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            cancelledArrival: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            cancelledDeparture: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            plannedArrival: {
                type: Sequelize.DATE,
                allowNull: true,
            },
            currentArrival: {
                type: Sequelize.DATE,
                allowNull: true,
            },
            plannedDeparture: {
                type: Sequelize.DATE,
                allowNull: true,
            },
            currentDeparture: {
                type: Sequelize.DATE,
                allowNull: true,
            },
            plannedTrack: {
                type: Sequelize.STRING(45),
                allowNull: true,
            },
            currentTrack: {
                type: Sequelize.STRING(45),
                allowNull: true,
            },
            arrived: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            departed: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            trainId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
            stationId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
            entryDataId: {
                type: Sequelize.UUID,
                allowNull: false,
            },
        });
        this.ORM.RouteEntry.sync().then(() => {
            if (callback)
                callback();
        });
    }

    /**
     * Create the traveler data table.
     * @param {Function=} callback - Called when the traveler data table is created
     * @protected
     */
    _createTravelerDataTable(callback) {
        this.ORM.TravelerData = this.sequelize.define('TravelerData', {
            id: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true,
                allowNull: false,
            },
            reservation: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            fee: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            noBoarding: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            rearTrainPartTerminates: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            shunting: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            specialTicket: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            replacementTransport: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            messages: {
                type: Sequelize.JSON,
                allowNull: true,
            },
        });
        this.ORM.TravelerData.sync().then(() => {
            if (callback)
                callback();
        });
    }

    /**
     * Create the positions table.
     * @param {Function=} callback - Called when the positions table is created
     * @protected
     */
    _createPositionsTable(callback) {
        this.ORM.Position = this.sequelize.define('Position', {
            id: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true,
                allowNull: false,
            },
            lat: {
                type: Sequelize.DOUBLE(10, 7),
                allowNull: false,
            },
            lon: {
                type: Sequelize.DOUBLE(10, 7),
                allowNull: false,
            },
            speed: {
                type: Sequelize.DOUBLE(6, 3),
                allowNull: false,
            },
            direction: {
                type: Sequelize.DOUBLE(5, 2),
                allowNull: false,
            },
            trainId: {
                type: Sequelize.UUID,
                allowNull: true,
            },
            rollingStockId: {
                type: Sequelize.UUID,
                allowNull: true,
            },
            measuredAt: {
                type: Sequelize.DATE,
                allowNull: false,
            },
        });
        this.ORM.Position.sync().then(() => {
            if (callback)
                callback();
        });
    }
}

module.exports = SQLiteFile;