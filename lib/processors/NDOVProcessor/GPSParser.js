const async = require('async');
const moment = require('moment');

const Parser = require('./Parser');

class GPSParser extends Parser {
    parse(message) {
        if (message.TreinLocation) {
            this._parseTrains(message.TreinLocation);
        }
    }

    _parseTrains(trains) {
        async.eachLimit(trains, 10, (train, callback) => {
            this._parseTrain(train, callback);
        });
    }

    _parseTrain(trainJSON, callback) {
        let trainNumber = trainJSON.TreinNummer;

        this.variables.ORM.Train.findOne({
            where: {
                trainNumber,
            }
        }).then(train => {
            if (train) {
                async.eachSeries(trainJSON.TreinMaterieelDelen, (stock, stockCallback) => {
                    if (parseInt(stock.Longitude) !== 0 && parseInt(stock.Latitude) !== 0 && (train.GPSAt === null || moment(stock.GpsDatumTijd).isAfter(train.GPSAt))) {
                        train.lat = stock.Latitude;
                        train.lon = stock.Longitude;
                        train.GPSAt = moment(stock.GpsDatumTijd).format();
                        train.save().then(() => {
                            stockCallback();
                        }).catch(err => {
                            stockCallback();
                        });
                    }
                    else {
                        stockCallback();
                    }
                }, err => {
                    callback();
                });
            }
            else {
                callback();
            }
        });
    }
}

module.exports = GPSParser;