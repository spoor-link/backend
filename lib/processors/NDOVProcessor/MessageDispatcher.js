const DVSParser = require('./DVSParser');
const DASParser = require('./DASParser');
const GPSParser = require('./GPSParser');

/**
 * Routes messages from NDOV to the correct parser. Which, in turn, parses the message and saves updates to the database.
 */
class MessageDispatcher {
    constructor(variables) {
        this.variables = variables;

        this.parsers = {};

        this._setParsers();
    }

    /**
     * Handle an incoming message.
     * @param {String} envelope - The envelope provided by the NDOV
     * @param {Object} message - The object containing the message
     */
    message(envelope, message) {
        let parser = this._getEnvelopeParser(envelope);

        if (parser)
            parser.parse(message);
    }

    /**
     * Set up all available parsers for message parsing.
     * @protected
     */
    _setParsers() {
        let parsers = this._getEnvelopeParsers();

        for (let envelope in parsers) {
            if (parsers.hasOwnProperty(envelope)) {
                let parser = parsers[envelope];

                this.parsers[envelope] = new parser(this.variables);
            }
        }
    }

    /**
     * Get the list of all parsers.
     * @return {*|{String: Parser}}
     * @protected
     */
    _getEnvelopeParsers() {
        return {
            '/RIG/InfoPlusDVSInterface4': DVSParser,
            '/RIG/InfoPlusDASInterface4': DASParser,
            '/RIG/NStreinpositiesInterface5': GPSParser,
        };
    }

    /**
     * Het the parser for a specified parser.
     * @param {String} envelope - The envelope
     * @return {Parser|Null} parser - The parser, if one was found
     * @private
     */
    _getEnvelopeParser(envelope) {
        if (typeof this.parsers[envelope] !== 'undefined') {
            return this.parsers[envelope];
        }

        return null;
    }
}

module.exports = MessageDispatcher;