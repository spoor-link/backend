const async = require('async');

/**
 * Base class for all NDOV parsers.
 * It provides shared methods, which are used on most/all parsers.
 */
class Parser {
    constructor(variables) {
        this.variables = variables;
    }

    /**
     * Parse a single message
     * @param {Object} message - A formatted JSON object containing the message
     * @abstract
     */
    parse(message) {
    }

    /**
     * Get the train and station ORM object, based on the train number and station code.
     * @param {String} trainNumber - The train number
     * @param {String} trainDate - An ISO formatted date string
     * @param {String} stationCode - The official code of the station
     * @param {trainAndStationCallback} callback
     * @protected
     */
    _getTrainAndStation(trainNumber, trainDate, stationCode, callback) {
        this.variables.ORM.Train.findOne({
            where: {
                trainId: trainNumber,
                trainDate,
            },
        }).then(train => {
            if (train) {
                this.variables.ORM.Station.findOne({
                    where: {
                        code: stationCode.toUpperCase(),
                    },
                }).then(station => {
                    if (station) {
                        this.variables.ORM.RouteEntry.findOne({
                            where: {
                                trainId: train.id,
                                stationId: station.id,
                            },
                        }).then(routeEntry => {
                            callback(train, routeEntry);
                        });
                    }
                    else {
                        callback(train, null);
                    }
                });
            }
            else {
                callback(null, null);
            }
        }).catch(e => {
            this.variables.logging.general.error(`Error while retrieving train and station for ${trainNumber}@${stationCode}`);
            this.variables.logging.general.error(e);
        });
    }

    /**
     * Updates route entries that came before the current route entry.
     * If the train has arrived at the current route entry AND the station is a major junction, all previous route entries are set to arrived and departed.
     * @param {Model} train - The ORM train object
     * @param {Model} routeEntry - The ORM route entry object
     * @param {moment.Moment} testDateTime - The datetime used to test the previous route entries
     * @protected
     */
    _updatePreviousRouteEntries(train, routeEntry, testDateTime) {
        routeEntry.getStation().then(station => {
            if (parseInt(station.type) >= 3) {
                this.variables.ORM.RouteEntry.findAll({
                    where: {
                        trainId: train.id,
                        id: {
                            [this.variables.ORM.Sequelize.Op.ne]: routeEntry.id,
                        },
                        [this.variables.ORM.Sequelize.Op.or]: [
                            {
                                plannedArrival: {
                                    [this.variables.ORM.Sequelize.Op.lt]: testDateTime,
                                },
                            },
                            {
                                plannedDeparture: {
                                    [this.variables.ORM.Sequelize.Op.lt]: testDateTime,
                                },
                            },
                        ],
                    },
                }).then(routeEntries => {
                    async.each(routeEntries, (previousRouteEntry, callback) => {
                        try {
                            previousRouteEntry.announced = true;

                            if (routeEntry.arrived) {
                                previousRouteEntry.arrived = true;
                                previousRouteEntry.departed = true;
                            }

                            previousRouteEntry.save();
                        }
                        catch (e) {
                            this.variables.logging.general.error(`Error while processing previous stations for ${trainNumber}`);
                            this.variables.logging.general.error(e);
                        }

                        callback();
                    });
                }).catch(e => {
                    this.variables.logging.general.error(`Error while processing previous stations for ${trainNumber}`);
                    this.variables.logging.general.error(e);
                });
            }
        }).catch(e => {
            this.variables.logging.general.error(`Error while processing previous stations for ${trainNumber}`);
            this.variables.logging.general.error(e);
        });
    }
}

/**
 * @callback trainAndStationCallback
 * @param {Model|Null} train - The ORM train object
 * @param {Model|Null} station - The ORM station object
 */

module.exports = Parser;