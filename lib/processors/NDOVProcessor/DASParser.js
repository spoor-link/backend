const moment = require('moment');

const Parser = require('./Parser');

/**
 * Parse incoming messages from the /RIG/InfoPlusDASInterface4 envelope.
 */
class DASParser extends Parser {
    /**
     * Parse a single message
     * @param {Object} message - A formatted JSON object containing the message
     */
    parse(message) {
        let trainNumber = message.DynamischeAankomstStaat.RitId;
        let trainDate = message.DynamischeAankomstStaat.RitDatum;
        let station = message.DynamischeAankomstStaat.RitStation.StationCode;

        this._getTrainAndStation(trainNumber, trainDate, station, (train, routeEntry) => {
            if (train && routeEntry) {
                let testDateTime = routeEntry.plannedArrival; // For selecting the previous route entries

                try {

                    // Train number
                    train.trainNumber = message.DynamischeAankomstStaat.TreinAankomst.TreinNummer;
                    train.save();

                    routeEntry.announced = true;

                    // Arrived
                    if (message.DynamischeAankomstStaat.TreinAankomst.TreinStatus === 2) {
                        routeEntry.arrived = true;

                        // Experimental: trains will be set to departed immediately at their terminal station
                        if (!routeEntry.plannedDeparture)
                            routeEntry.departed = true;
                    }

                    // Arrival time
                    routeEntry.plannedArrival = moment(message.DynamischeAankomstStaat.TreinAankomst.AankomstTijd.Gepland);
                    routeEntry.currentArrival = moment(message.DynamischeAankomstStaat.TreinAankomst.AankomstTijd.Actueel);

                    // Track
                    let plannedTrack = message.DynamischeAankomstStaat.TreinAankomst.TreinAankomstSpoor.Gepland.SpoorNummer;
                    if (message.DynamischeAankomstStaat.TreinAankomst.TreinAankomstSpoor.Gepland.SpoorFase) // Track phase
                        plannedTrack += message.DynamischeAankomstStaat.TreinAankomst.TreinAankomstSpoor.Gepland.SpoorFase;

                    let currentTrack = message.DynamischeAankomstStaat.TreinAankomst.TreinAankomstSpoor.Actueel.SpoorNummer;
                    if (message.DynamischeAankomstStaat.TreinAankomst.TreinAankomstSpoor.Actueel.SpoorFase) // Track phase
                        currentTrack += message.DynamischeAankomstStaat.TreinAankomst.TreinAankomstSpoor.Actueel.SpoorFase;

                    routeEntry.plannedTrack = plannedTrack;
                    routeEntry.currentTrack = currentTrack;

                    this.cancelledArrival = false; // There should be a change if it is cancelled

                    // Parse status codes
                    if (typeof message.DynamischeAankomstStaat.TreinAankomst.WijzigingHerkomst !== 'undefined') {
                        for (let i = 0; i < message.DynamischeAankomstStaat.TreinAankomst.WijzigingHerkomst.length; i++) {
                            // Cancelled arrival
                            if (message.DynamischeAankomstStaat.TreinAankomst.WijzigingHerkomst[i].WijzigingType === '39') {
                                routeEntry.cancelledArrival = true;
                            }
                        }
                    }

                    routeEntry.save();
                }
                catch (e) {
                    this.variables.logging.general.error(`DAS: Error while processing ${trainNumber}@${station}`);
                    this.variables.logging.general.error(JSON.stringify(message));
                    this.variables.logging.general.error(e);
                }

                this._updatePreviousRouteEntries(train, routeEntry, testDateTime);
            }
        });
    }
}

module.exports = DASParser;