const moment = require('moment');

const Parser = require('./Parser');

/**
 * Parse incoming messages from the /RIG/InfoPlusDVSInterface4 envelope.
 */
class DVSParser extends Parser {
    /**
     * Parse a single message
     * @param {Object} message - A formatted JSON object containing the message
     */
    parse(message) {
        let trainNumber = message.DynamischeVertrekStaat.RitId;
        let trainDate = message.DynamischeVertrekStaat.RitDatum;
        let station = message.DynamischeVertrekStaat.RitStation.StationCode;

        this._getTrainAndStation(trainNumber, trainDate, station, (train, routeEntry) => {
            if (train && routeEntry) {
                try {
                    train.trainNumber = message.DynamischeVertrekStaat.Trein.TreinNummer;
                    train.save();

                    routeEntry.announced = true;

                    if (message.DynamischeVertrekStaat.Trein.TreinStatus === 2 || message.DynamischeVertrekStaat.Trein.TreinStatus === 5) {
                        routeEntry.arrived = true;
                    }
                    if (message.DynamischeVertrekStaat.Trein.TreinStatus === 5) {
                        routeEntry.departed = true;
                    }

                    routeEntry.plannedDeparture = moment(message.DynamischeVertrekStaat.Trein.VertrekTijd.Gepland);
                    routeEntry.currentDeparture = moment(message.DynamischeVertrekStaat.Trein.VertrekTijd.Actueel);

                    // If the current departure is before the current arrival (due to delays), we'll set the current arrival as well.
                    if (moment(routeEntry.currentDeparture).isBefore(moment(routeEntry.currentArrival)))
                        routeEntry.currentArrival = routeEntry.currentDeparture;

                    let plannedTrack = message.DynamischeVertrekStaat.Trein.TreinVertrekSpoor.Gepland.SpoorNummer;
                    if (message.DynamischeVertrekStaat.Trein.TreinVertrekSpoor.Gepland.SpoorFase) // Track phase
                        plannedTrack += message.DynamischeVertrekStaat.Trein.TreinVertrekSpoor.Gepland.SpoorFase;

                    let currentTrack = message.DynamischeVertrekStaat.Trein.TreinVertrekSpoor.Actueel.SpoorNummer;
                    if (message.DynamischeVertrekStaat.Trein.TreinVertrekSpoor.Actueel.SpoorFase) // Track phase
                        currentTrack += message.DynamischeVertrekStaat.Trein.TreinVertrekSpoor.Actueel.SpoorFase;

                    routeEntry.plannedTrack = plannedTrack;
                    routeEntry.currentTrack = currentTrack;

                    routeEntry.cancelledDeparture = false; // There should be a change if it is cancelled

                    // Parse status codes
                    if (typeof message.DynamischeVertrekStaat.Trein.Wijziging !== 'undefined') {
                        for (let i = 0; i < message.DynamischeVertrekStaat.Trein.Wijziging.length; i++) {
                            // Stop type change
                            if (message.DynamischeVertrekStaat.Trein.Wijziging[i].WijzigingType === '30') {
                                console.log(JSON.stringify(message));
                            }

                            // cancelled departure
                            if (message.DynamischeVertrekStaat.Trein.Wijziging[i].WijzigingType === '32') {
                                routeEntry.cancelledDeparture = true;
                            }
                        }
                    }

                    routeEntry.save();
                }
                catch (e) {
                    this.variables.logging.general.error(`DVS: Error while processing ${trainNumber}@${station}`);
                    this.variables.logging.general.error(JSON.stringify(message));
                    this.variables.logging.general.error(e);
                }

                this._updatePreviousRouteEntries(train, routeEntry, routeEntry.plannedDeparture);
            }
        });
    }
}

module.exports = DVSParser;