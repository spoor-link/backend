const Processor = require('../Processor');
const Timetable = require('../Timetable');
const getDistance = require('../distance').getDistance;
const moment = require('moment');
const async = require('async');
const utf8 = require('utf8');
const path = require('path');
const fs = require('fs');

/**
 * Processor for importing the planned timetable into the database
 */
class NewTimetableProcessor extends Processor {
    getValidation() {
        return !!(this.variables.ORM.Train && this.variables.ORM.RouteEntry && process.env.timetable_folder);
    }

    getSchedules() {
        return [
            '10 0 1,2,17,19,20 * * *', // 10 seconds past the hour, because on top of every hour/minute, a lot of stuff happens
        ];
    }

    /**
     * Import the timetable of today before the main app logic starts, to make sure it's available
     * @param callback
     */
    preRun(callback) {
        // Import the timetable of today, to make sure it's imported
        this._importTimetable(moment().format('YYYY-MM-DD'), () => {
            if (callback)
                callback();
        });
    }

    /**
     * Import the timetable of tomorrow on specified datetimes.
     * @param fireDate
     */
    runSchedule(fireDate) {
        let date = fireDate.add(14, 'hours').format('YYYY-MM-DD'); // We add 14 hours, because after midnight it adds the timetable for that same day

        this._importTimetable(date);
    }

    /**
     * Start importing of the timetable.
     * @param {String} date - The date for which day the timetable should be imported
     * @param {Function=} callback - Called after the timetable has completely finished importing
     * @protected
     */
    _importTimetable(date, callback) {
        let timetable = new ImportTimetable(this.variables, date);
        timetable.import(() => {
            if (callback)
                callback();
        });
    }
}

/**
 * Retrieves all information about the timetable and adds it to the database
 */
class ImportTimetable {
    constructor(variables, date) {
        this.variables = variables;
        this.date = date;
        this.data = {};
        this.DBIDs = {};
    }

    /**
     * Bootstraps the importing process.
     * @param {Function} callback - Called after the timetable has completely finished importing
     */
    import(callback) {
        this.variables.logging.general.info(`Importing timetable for ${this.date}`);

        // Get all timetable information
        this._getTimetable(() => {
            async.series([
                callback => {
                    // Import countries
                    this._importCountries(callback);
                },
                callback => {
                    // Import companies
                    this._importCompanies(callback);
                },
                callback => {
                    // Import stations
                    this._importStations(callback);
                },
                callback => {
                    // Import train types
                    this._importTrainTypes(callback);
                },
                callback => {
                    // Get the trains for the specified day that are already imported
                    this._getExistingTrains(callback);
                },
                callback => {
                    // Import the timetable
                    this._importTimetable(callback);
                },
                callback => {
                    // Create the route finding file
                    this._createStatConn(callback);
                },
            ], err => {
                if (err)
                    this.variables.logging.general.error(err);

                if (callback)
                    callback();
            });
        })
    }

    /**
     * Get the timetable data
     * @param {Function} callback - Called after the raw timetable is imported into the member variable
     * @protected
     */
    _getTimetable(callback) {
        let timetable = new Timetable();
        timetable.findAll({
            date: this.date,
        }, (data, err) => {
            if (err)
                this.variables.logging.general.error(err);

            this.data = data;

            callback();
        });
    }

    /**
     * Import all countries that are not imported yet
     * @param {Function} callback - Called after all companies are checked and, if required, imported
     * @protected
     */
    _importCountries(callback) {
        this.DBIDs.countries = {};

        async.each(this.data.countries, (country, callback) => {
            this.variables.ORM.Country.findOrCreate({
                where: {
                    code: country.code,
                },
                defaults: {
                    fullName: country.fullName,
                },
            }).spread((country, created) => {
                this.DBIDs.countries[country.code] = country.id;

                callback();
            }).catch(err => {
                this.variables.logging.general.error(`Error while importing country ${country.code}`);
                this.variables.logging.general.error(country);
                this.variables.logging.general.error(err);

                callback();
            });
        }, err => {
            if (err)
                this.variables.logging.general.error(err);

            if (callback)
                callback();
        });
    }

    /**
     * Import all companies that are not imported yet
     * @param {Function} callback - Called after all companies are checked and, if required, imported
     * @protected
     */
    _importCompanies(callback) {
        this.DBIDs.companies = {};

        async.each(this.data.companies, (company, callback) => {
            this.variables.ORM.Company.findOrCreate({
                where: {
                    companyCode: company.code,
                },
                defaults: {
                    fullName: company.fullName,
                    turnOfDay: company.turnOfDay,
                },
            }).spread((DBCompany, created) => {
                this.DBIDs.companies[company.ID] = DBCompany.id;

                callback();
            }).catch(err => {
                this.variables.logging.general.error(`Error while importing company ${company.code}`);
                this.variables.logging.general.error(company);
                this.variables.logging.general.error(err);

                callback();
            });
        }, err => {
            if (err)
                this.variables.logging.general.error(err);

            if (callback)
                callback();
        });
    }

    /**
     * Import all stations that are not imported yet
     * @param {Function} callback - Called after all stations are checked and, if required, imported
     * @protected
     */
    _importStations(callback) {
        this.DBIDs.stations = {};

        async.each(this.data.stations, (station, callback) => {
            if (station.lat && station.lon && station.shortName && station.middleName && station.longName) {
                this.variables.ORM.Station.findOrCreate({
                    where: {
                        code: station.code,
                    },
                    defaults: {
                        changeTime: station.changeTime,
                        lat: station.lat,
                        lon: station.lon,
                        shortName: station.shortName,
                        middleName: station.middleName,
                        longName: station.longName,
                        type: station.type,
                        countryId: this.DBIDs.countries[station.countryCode],
                    },
                }).spread((station, created) => {
                    this.DBIDs.stations[station.code] = station.id;

                    callback();
                }).catch(err => {
                    this.variables.logging.general.error(`Error while importing station ${station.code}`);
                    this.variables.logging.general.error(station);
                    this.variables.logging.general.error(err);

                    callback();
                });
            }
            else {
                callback();
            }
        }, err => {
            if (err)
                this.variables.logging.general.error('An error occurred while processing stations');
            //this.variables.logging.general.error(err);

            if (callback)
                callback();
        });
    }

    /**
     * Import all train types that are not imported yet
     * @param {Function} callback - Called after all train types are checked and, if required, imported
     * @protected
     */
    _importTrainTypes(callback) {
        this.DBIDs.trainTypes = {};

        async.each(this.data.trainTypes, (trainType, callback) => {
            this.variables.ORM.TrainType.findOrCreate({
                where: {
                    code: trainType.code,
                },
                defaults: {
                    fullName: trainType.fullName,
                    passengers: trainType.passengers,
                },
            }).spread((trainType, created) => {
                this.DBIDs.trainTypes[trainType.code] = trainType.id;

                callback();
            }).catch(err => {
                this.variables.logging.general.error(`Error while importing train type ${trainType.code}`);
                this.variables.logging.general.error(trainType);
                this.variables.logging.general.error(err);

                callback();
            });
        }, err => {
            if (err)
                this.variables.logging.general.error('An error occurred while processing train types');
            //this.variables.logging.general.error(err);

            if (callback)
                callback();
        });
    }

    /**
     * Get all trains that are already imported for the specified day
     * @param {Function} callback - Called after all existing trains are checked and, if required, imported
     * @protected
     */
    _getExistingTrains(callback) {
        this.DBIDs.trains = [];

        this.variables.ORM.Train.findAll({
            where: {
                trainDate: this.date,
            },
        }).then(trains => {
            async.each(trains, (train, callback) => {
                this.DBIDs.trains.push(train.trainId);

                callback();
            }, err => {
                if (err)
                    this.variables.logging.general.error(err);

                callback();
            });
        }).catch(err => {
            this.variables.logging.general.error('Error while querying existing trains');
            //this.variables.logging.general.error(err);
        });
    }

    /**
     * Import all trains that are not imported yet
     * @param {Function} callback - Called after the timetable are checked and, if required, imported
     * @private
     */
    _importTimetable(callback) {
        if (this.DBIDs.trains.length < 100)
            this.variables.logging.general.info('Importing will take a while...');

        let trainCount = 0; // Used for displaying console information

        // Loop through each train from the raw data
        async.eachSeries(this.data.timetable, (train, trainCallback) => {
            if (!this.DBIDs.trains.includes(train.trainNumber.trainNumber)) { // If the train doesn't exist yet
                if ((trainCount % 100) === 0 && trainCount !== 0)
                    this.variables.logging.general.info(`Imported ${trainCount} trains`);

                trainCount++;

                let startStationId = this.DBIDs.stations[train.routeEntries[0].station];
                let destinationStationId = this.DBIDs.stations[train.routeEntries[train.routeEntries.length - 1].station];
                let companyId = this.DBIDs.companies[train.trainNumber.company];
                let trainTypeId = this.DBIDs.trainTypes[train.trainTypes[0].trainType];

                // Create the train in the database
                this.variables.ORM.Train.create({
                    trainId: train.trainNumber.trainNumber,
                    trainNumber: train.trainNumber.trainNumber,
                    trainDate: this.date,
                    comments: train.trainNumber.trainName || null,
                    plannedStartId: startStationId,
                    currentStartId: startStationId,
                    plannedDestinationId: destinationStationId,
                    currentDestinationId: destinationStationId,
                    companyId,
                    trainTypeId,
                }).then(DBTrain => {
                    // Import the route entries
                    async.eachSeries(train.routeEntries, (routeEntry, routeEntryCallback) => {
                        //TODO: incorporate attributes into this
                        this.variables.ORM.TravelerData.create({}).then(DBTravelerData => {
                            let stationId = this.DBIDs.stations[routeEntry.station];

                            if (routeEntry.arrival !== null)
                                if (routeEntry.arrival.isMoment === true)
                                    routeEntry.arrival = routeEntry.arrival.format();

                            if (routeEntry.departure !== null)
                                if (routeEntry.departure.isMoment === true)
                                    routeEntry.departure = routeEntry.departure.format();

                            this.variables.ORM.RouteEntry.create({
                                stopType: routeEntry.stopType,
                                plannedArrival: routeEntry.arrival,
                                currentArrival: routeEntry.arrival,
                                plannedDeparture: routeEntry.departure,
                                currentDeparture: routeEntry.departure,
                                plannedTrack: routeEntry.track,
                                currentTrack: routeEntry.track,
                                trainId: DBTrain.id,
                                stationId,
                                entryDataId: DBTravelerData.id,
                            }).then(DBRouteEntry => {
                                routeEntryCallback();
                            }).catch(err => {
                                this.variables.logging.general.error(`Error while creating the route entry for ${train.trainNumber.trainNumber}:${routeEntry.station}`);
                                //this.variables.logging.general.error(err);

                                routeEntryCallback();
                            })
                        }).catch(err => {
                            this.variables.logging.general.error(`Error while creating traveler data for ${train.trainNumber.trainNumber}:${routeEntry.station}`);
                            //this.variables.logging.general.error(err);

                            routeEntryCallback();
                        });
                    }, err => {
                        if (err)
                            this.variables.logging.general.error(err);

                        if (trainCallback)
                            trainCallback();
                    });
                }).catch(err => {
                    this.variables.logging.general.error(`Error while creating train ${train.trainNumber.trainNumber}`);
                    //this.variables.logging.general.error(err);

                    trainCallback();
                });
            }
            else {
                trainCallback();
            }
        }, err => {
            if (err)
                this.variables.logging.general.error('An error occurred while processing trains');
            //this.variables.logging.general.error(err);

            if (trainCount === 0) {
                this.variables.logging.general.info('No trains needed to import');
            } else {
                this.variables.logging.general.info(`Imported a total of ${trainCount} trains`);
            }

            if (callback)
                callback();
        });
    }

    /**
     * Create the file used for route finding
     * @param {Function} callback - Called after the file with station connections is created
     * @private
     */
    _createStatConn(callback) {
        let stations = {};
        let exportStatConn = {};

        this.variables.ORM.Station.findAll({}).then(DBStations => {
            // Loop through each station and save it to the stations variable
            async.each(DBStations, (station, callback) => {
                stations[station.code] = station;
                callback();
            }, err => {
                // Loop through all station connections
                async.each(this.data.statConns, (statConn, callback) => {
                    let from = stations[statConn.from];
                    let to = stations[statConn.to];

                    // If both from and to stations are available
                    if (typeof from !== 'undefined' && typeof to !== 'undefined') {
                        // Calculate the distance between the stations (as the crow flies, this doesn't use the railway tracks)
                        let distance = getDistance(
                            {
                                latitude: from.lat,
                                longitude: from.lon,
                            },
                            {
                                latitude: to.lat,
                                longitude: to.lon,
                            }
                        );

                        // Add the connection to the list of exported station connections
                        if (typeof exportStatConn[from.code] === 'undefined')
                            exportStatConn[from.code] = {};

                        exportStatConn[from.code][to.code] = distance;
                    }

                    callback();
                }, err => {
                    if (err)
                        this.variables.logging.general.error(err);

                    // Write the list of station connection to file as a JSON string
                    let filePath = path.join(process.cwd(), process.env.timetable_statconn);
                    fs.writeFile(filePath, JSON.stringify(exportStatConn));

                    callback();
                });
            });
        });
    }
}

module.exports = NewTimetableProcessor;