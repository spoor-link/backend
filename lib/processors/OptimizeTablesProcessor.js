const async = require('async');
const Processor = require('../Processor');

/**
 * Once a day, all tables are optimized using the "MYSQL OPTIMIZE" query.
 * This reduces it's file size and optimizes indexes, both making it much quicker.
 */
class OptimizeTablesProcessor extends Processor {
    getValidation() {
        return true;
    }

    getSchedules() {
        return [
            '8 0 4 * * *',
        ];
    }

    runSchedule(fireDate) {
        this._optimizeTables();
    }

    /**
     * Run the optimization for each table.
     * @param {Function=} callback - Called after it has finished optimizing the tables
     * @protected
     */
    _optimizeTables(callback) {
        this.variables.logging.general.info('Optimizing SQL tables');

        // All tables which are going to get optimized
        let tables = [
            'companies',
            'countries',
            'positions',
            'rollingStocks',
            'routeEntries',
            'routeEntryHasRollingStocks',
            'stations',
            'trains',
            'trainTypes',
            'travelerData',
        ];

        async.eachSeries(tables, (table, tableCallback) => {
            this.variables.logging.general.info(`Optimizing ${table}`);

            this.variables.ORM.sequelize.query(`OPTIMIZE TABLE ${table}`, null, {raw: true}).then(() => {
                tableCallback();
            });
        }, err => {
            if (callback)
                callback();
        });
    }
}

module.exports = OptimizeTablesProcessor;