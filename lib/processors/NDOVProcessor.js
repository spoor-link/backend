const Processor = require('../Processor');
const NDOVMessages = require('ndov-messages');
const MessageDispatcher = require('./NDOVProcessor/MessageDispatcher');

/**
 * Receives information from the NDOV service and updates the database with live information.
 */
class NDOVProcessor extends Processor {
    constructor() {
        super();

        this.statistics = {};
    }

    getValidation() {
        return true;
    }

    getSchedules() {
        return [
            '0 */5 * * * *',
        ];
    }

    run() {
        this._startDispatcher();
        this._startListener();
    }

    /**
     * Display statistics every 5 minutes about the incoming messages.
     * @param {moment.Moment} fireDate - The datetime object from the moment it fired
     */
    runSchedule(fireDate) {
        // Show statistics of the incoming messages

        this.variables.logging.general.info('5-minute statistics of incoming messages:');
        for (let envelope in this.statistics) {
            if (!this.statistics.hasOwnProperty(envelope)) continue;

            let hits = this.statistics[envelope];
            this.variables.logging.general.info(`${envelope}: ${hits}`);
        }

        this.statistics = {};
    }

    /**
     * Starts the listener
     * @protected
     */
    _startListener() {
        this.messages = new NDOVMessages();

        this.messages.addListener('NDOV', process.env.zeromq_address, '/');

        this.messages.on('message', (envelope, message) => {
            // Save the hit for statistics
            if (typeof this.statistics[envelope] === 'undefined')
                this.statistics[envelope] = 0;
            this.statistics[envelope]++;

            // Run the parser
            this._parseMessage(envelope, message);
        });
    }

    /**
     * Start the message dispatcher. This handles all incoming messages.
     * @protected
     */
    _startDispatcher() {
        this.dispatcher = new MessageDispatcher(this.variables);
    }

    /**
     * Parse a message. It is sent to the message dispatcher for processing
     * @param {String} envelope - The subscription envelope provided by the data source
     * @param {Object} message - A JSON object containing the message
     * @protected
     */
    _parseMessage(envelope, message) {
        this.dispatcher.message(envelope, message);
    }
}


module.exports = NDOVProcessor;