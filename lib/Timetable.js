const download = require('download');
const unzip = require('unzip');
const path = require('path');
const fs = require('fs');
const rimraf = require('rimraf');
const async = require('async');
const moment = require('moment');
const xml2js = require('xml2js');
const request = require('request');
const uuidv4 = require('uuid/v4');

const IFFTimetable = require('./IFF/IFFTimetable');
const IFFCompany = require('./IFF/IFFCompany');
const IFFCountry = require('./IFF/IFFCountry');
const IFFFootnote = require('./IFF/IFFFootnote');
const IFFStatConn = require('./IFF/IFFStatConn');
const IFFStation = require('./IFF/IFFStation');
const IFFTransportMode = require('./IFF/IFFTransportMode');

/**
 * Downloads and reads all sources related to the train timetable.
 * Parsed data is stored in it's member variables, available for further processing.
 * It required certain environment variables being set.
 */
class Timetable {
    constructor() {
        this.companies = {};
        this.countries = {};
        this.footnotes = {};
        this.statConns = {};
        this.stations = {};
        this.timetable = {};
        this.trainTypes = {};

        this.timetableDir = null;
    }

    /**
     * Get all trains and related information
     * @param {Object} opts - JSON object of all options
     * @param functionCallback
     */
    findAll(opts, functionCallback) {

        opts = this._parseOptions(opts);

        // Parse all required raw data into JSON objects. The functions can filter the data based on the provided options.
        // this._getIFFTimetable is called last, because it filters based on available footnotes
        async.series([
            (callback) => {
                this._downloadJSONStations(callback);
            },
            (callback) => {
                this._getIFFFiles(callback);
            },
            (callback) => {
                this._getIFFCompanies(opts, callback);
            },
            (callback) => {
                this._getIFFCountries(opts, callback);
            },
            (callback) => {
                this._getIFFFootnotes(opts, callback);
            },
            (callback) => {
                this._getIFFStatConns(opts, callback);
            },
            (callback) => {
                this._getIFFStations(opts, callback);
            },
            (callback) => {
                this._getJSONStations(opts, callback);
            },
            (callback) => {
                this._getIFFTimetable(opts, callback);
            },
            (callback) => {
                this._getIFFTransportModes(opts, callback);
            },
        ], (err) => {
            functionCallback({
                    trainTypes: this.trainTypes,
                    countries: this.countries,
                    companies: this.companies,
                    footnotes: this.footnotes,
                    statConns: this.statConns,
                    stations: this.stations,
                    timetable: this.timetable,
                },
                err);
        });
    }

    /**
     * Sanitizes the options given to the object instance
     * @param {Object} opts - JSON object of all options
     * @return {Object} Sanitized options
     * @protected
     */
    _parseOptions(opts) {
        if (typeof opts !== 'object')
            opts = {};

        if (typeof opts.date === 'undefined')
            opts.date = moment().format('YYYY-MM-DD');

        return opts;
    }

    /**
     * Get all companies from the IFF source
     * @param {Object} opts - JSON object of all options
     * @param {Function} callback - called after the parsing is complete
     * @protected
     */
    _getIFFCompanies(opts, callback) {
        let companies = new IFFCompany();

        let companyFile = path.join(this.timetableDir, 'company.dat');
        companies.readFile(companyFile, parsedCompanies => {
            this.companies = parsedCompanies;

            if (callback)
                callback();
        });
    }

    /**
     * Get all countries from the IFF source
     * @param {Object} opts - JSON object of all options
     * @param {Function} callback - called after the parsing is complete
     * @protected
     */
    _getIFFCountries(opts, callback) {
        let countries = new IFFCountry();

        let countryFile = path.join(this.timetableDir, 'country.dat');
        countries.readFile(countryFile, parsedCountries => {
            this.countries = parsedCountries;

            if (callback)
                callback();
        });
    }

    /**
     * Get all footnotes from the IFF source
     * @param {Object} opts - JSON object of all options
     * @param {Function} callback - called after the parsing is complete
     * @protected
     */
    _getIFFFootnotes(opts, callback) {
        let footnotes = new IFFFootnote({
            filterDate: opts.date,
        });

        let footnoteFile = path.join(this.timetableDir, 'footnote.dat');
        footnotes.readFile(footnoteFile, notes => {
            this.footnotes = notes;

            if (callback)
                callback();
        });
    }

    /**
     * Get all station connections from the IFF source
     * @param {Object} opts - JSON object of all options
     * @param {Function} callback - called after the parsing is complete
     * @protected
     */
    _getIFFStatConns(opts, callback) {
        let statConns = new IFFStatConn();

        let statConnFile = path.join(this.timetableDir, 'statconn.dat');
        statConns.readFile(statConnFile, parsedStatConns => {
            this.statConns = parsedStatConns;

            if (callback)
                callback();
        });
    }

    /**
     * Get all stations from the IFF source
     * @param {Object} opts - JSON object of all options
     * @param {Function} callback - called after the parsing is complete
     * @protected
     */
    _getIFFStations(opts, callback) {
        let stations = new IFFStation();

        let stationsFile = path.join(this.timetableDir, 'stations.dat');
        stations.readFile(stationsFile, parsedStations => {
            this.stations = parsedStations;

            if (callback)
                callback();
        });
    }

    /**
     * Get all stations from the XML source
     * @param {Object} opts - JSON object of all options
     * @param {Function} callback - called after the parsing is complete
     * @protected
     */
    _getJSONStations(opts, callback) {
        let stationsFile = path.join(process.cwd(), process.env.timetable_stations);

        fs.readFile(stationsFile, (err, data) => {
            if (err)
                console.error(err);

            // Parse JSON into an object
            let JSONData = JSON.parse(data);

            // Loop through each station, match it to an existing station and update it's information
            try {
                async.each(JSONData.payload, (station, callback) => {
                    for (let i = 0, len = this.stations.length; i < len; i++) {
                        if (this.stations[i].code === station.code) {
                            this.stations[i].shortName = station.namen.kort;
                            this.stations[i].middleName = station.namen.middel;
                            this.stations[i].longName = station.namen.lang;
                            delete this.stations[i].fullName;

                            this.stations[i].lat = parseFloat(station.lat);
                            this.stations[i].lon = parseFloat(station.lng);

                            this.stations[i].UIC = parseInt(station.UICCode);

                            switch (station.stationType) {
                                case 'FACULTATIEF_STATION':
                                    this.stations[i].type = -1;
                                    break;
                                case 'KNOOPPUNT_STOPTREIN_STATION':
                                    this.stations[i].type = 1;
                                    break;
                                case 'SNELTREIN_STATION':
                                    this.stations[i].type = 2;
                                    break;
                                case 'KNOOPPUNT_SNELTREIN_STATION':
                                    this.stations[i].type = 3;
                                    break;
                                case 'INTERCITY_STATION':
                                    this.stations[i].type = 4;
                                    break;
                                case 'KNOOPPUNT_INTERCITY_STATION':
                                    this.stations[i].type = 5;
                                    break;
                                case 'MEGA_STATION':
                                    this.stations[i].type = 6;
                                    break;
                                default: // Any other values is set to the equivalent of "STOPTREIN_STATION"
                                    this.stations[i].type = 0;
                                    break;
                            }
                        }
                    }

                    callback();
                }, () => {
                    if (callback)
                        callback();
                });
            } catch (e) {
                //console.error(e);
                if (callback)
                    callback();
            }
        });
    }

    /**
     * Get all timetable from the IFF source
     * @param {Object} opts - JSON object of all options
     * @param {Function} callback - called after the parsing is complete
     * @protected
     */
    _getIFFTimetable(opts, callback) {
        let IDs = [];

        for (let i = 0, len = this.footnotes.length; i < len; i++) {
            IDs.push(this.footnotes[i].ID);
        }

        let rawTimetable = new IFFTimetable({
            footnoteFilter: IDs,
            date: opts.date,
        });
        let timetableFile = path.join(this.timetableDir, 'timetbls.dat');

        rawTimetable.readFile(timetableFile, timetable => {
            this.timetable = timetable;

            if (callback)
                callback();
        });
    }

    /**
     * Get all transport modes from the IFF source
     * @param {Object} opts - JSON object of all options
     * @param {Function} callback - called after the parsing is complete
     * @protected
     */
    _getIFFTransportModes(opts, callback) {
        let transportModes = new IFFTransportMode();

        let transportModesFile = path.join(this.timetableDir, 'trnsmode.dat');
        transportModes.readFile(transportModesFile, parsedTransportModes => {
            this.trainTypes = parsedTransportModes;

            // Add some (for now) hardcoded train types
            this.trainTypes.push({
                code: 'GO',
                fullName: 'Goederen',
                passengers: false,
            });
            this.trainTypes.push({
                code: 'LM',
                fullName: 'Ledig materieel',
                passengers: false,
            });

            if (callback)
                callback();
        });
    }

    /**
     * Downloads the list of stations from the NS API
     * @param {Function} callback - called after downloading is complete
     * @protected
     */
    _downloadJSONStations(callback) {
        let APIURL = 'https://gateway.apiportal.ns.nl/reisinformatie-api/api/v2/stations';

        let options = {
            url: APIURL,
            headers: {
                'Ocp-Apim-Subscription-Key': process.env.NS_api_password,
            },
        };

        request(options)
            .pipe(fs.createWriteStream(process.env.timetable_stations)
                .on('close', () => {
                    if (callback)
                        callback();
                }));
    }

    /**
     * Downloads the timetable from NDOV
     * @param {Function} callback - called after downloading is complete
     * @protected
     */
    _getIFFFiles(callback) {
        let timetableRootDir = path.join(process.cwd(), process.env.timetable_folder);
        let timetableFile = path.join(timetableRootDir, 'timetable.zip');
        this.timetableDir = path.join(timetableRootDir, uuidv4());

        // If the root directory doesn't exist yet, create it
        if (!fs.existsSync(timetableRootDir))
            fs.mkdirSync(timetableRootDir);

        // Remove old timetable directories
        this._cleanTimetablesFolder(timetableRootDir);

        // Now make the directory used for storing the timetable temporary
        fs.mkdirSync(this.timetableDir);

        // Download the new timetable and extract it
        this._downloadTimetable(timetableFile, () => {
            fs.createReadStream(timetableFile)
                .pipe(unzip.Extract({path: this.timetableDir}))
                .on('close', () => {
                    if (callback)
                        callback();
                });
        });
    }

    /**
     * Synchronous function to remove as many old timetable directories as possible
     * @param {String} timetableFolder - The root folder of the timetables
     * @protected
     */
    _cleanTimetablesFolder(timetableFolder) {
        try {
            // Get a list of all directories within
            const isDirectory = source => fs.lstatSync(source).isDirectory();
            const getDirectories = source =>
                fs.readdirSync(source).map(name => path.join(source, name)).filter(isDirectory);

            let directories = getDirectories(timetableFolder);

            for (let i = 0; i < directories.length; i++) {
                try {
                    rimraf.sync(directories[i], {});
                }
                catch (e) {
                    console.error(e);
                }
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    /**
     * Download a new timetable from the public data source
     * @param {String} destFile - The destination path where the timetable will be stored
     * @param {Function} callback - Callback when downloading has completed or failed
     * @protected
     */
    _downloadTimetable(destFile, callback) {
        download(process.env.timetable_url).then(data => {
            fs.writeFileSync(destFile, data);

            if (callback)
                callback();
        }).catch(err => {
            console.error(err);

            if (callback)
                callback();
        });
    }
}

module.exports = Timetable;